package com.sbc.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.sbc.movil.R;
import com.sbc.util.dao.ReloadListener;

public class ProgressBarUtil {

    private ProgressBar progressBar;
    private ReloadListener reloadListener;
    private View mensaje;


    public ProgressBarUtil(Context context, ViewGroup viewGroup, ReloadListener reloadListenerP) {

        this.reloadListener = reloadListenerP;

        progressBar = new ProgressBar(context, null, android.R.attr.progressBarStyleSmall);
        progressBar.setIndeterminate(true);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        viewGroup.addView(progressBar, params);
        progressBar.bringToFront();
        progressBar.setVisibility(View.GONE);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mensaje = inflater.inflate(R.layout.message, viewGroup, false);

        mensaje.setVisibility(View.GONE);

        viewGroup.addView(mensaje);


        Button button = (Button) mensaje.findViewById(R.id.buttonMessage);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reloadListener.reload();
            }
        });
    }

    public void start() {

        mensaje.setVisibility(View.GONE);


        progressBar.setVisibility(View.VISIBLE);

    }

    public void stop() {

        mensaje.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);

    }

    public void stopOnError() {
        mensaje.setVisibility(View.VISIBLE);


        progressBar.setVisibility(View.GONE);


    }


}
