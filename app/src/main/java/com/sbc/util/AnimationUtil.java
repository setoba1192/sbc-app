package com.sbc.util;

import android.view.View;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

/**
 * Created by JACS1 on 20/04/2017.
 */

public class AnimationUtil {


    public static void animar(View v){

        YoYo.with(Techniques.Shake)
                .duration(700)
                .playOn(v);

    }
}
