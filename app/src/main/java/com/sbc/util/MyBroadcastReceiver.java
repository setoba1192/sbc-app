package com.sbc.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.widget.Toast;

import com.sbc.model.app.Recordatorio;
import com.sbc.movil.R;
import com.sbc.movil.SplashActivity;
import com.sbc.movil.app.PrincipalActivity;

import java.util.Calendar;
import java.util.Date;

public class MyBroadcastReceiver extends BroadcastReceiver {
	MediaPlayer mp;
	@Override
	public void onReceive(Context context, Intent intent) {
		mp=MediaPlayer.create(context, R.raw.tono	);
		mp.start();



		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, "default")
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle("Recordatorio "+intent.getStringExtra("dia"))
				.setContentText("No olvides realizar observación de tu área!")
				.setPriority(NotificationCompat.PRIORITY_DEFAULT).setCategory ( NotificationCompat.CATEGORY_ALARM);
		//Vibration
		mBuilder.setVibrate(new long[] { 800, 800});
		//LED
		mBuilder.setLights(Color.RED, 3000, 3000);

        Intent notifyIntent = new Intent(context, SplashActivity.class);
// Set the Activity to start in a new, empty task
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
// Create the PendingIntent
        PendingIntent notifyPendingIntent = PendingIntent.getActivity(
                context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT
        );

        mBuilder.setContentIntent(notifyPendingIntent);

		NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);



		//Re schedule Alarm
		Recordatorio recordatorio = JsonTransformer.fromJSON(intent.getStringExtra("recordatorio"), Recordatorio.class);

		Intent intentReceiver = new Intent(context, MyBroadcastReceiver.class);


			Date dat = new Date();

			Calendar cal_alarm = Calendar.getInstance();
			Calendar cal_now = Calendar.getInstance();
			cal_now.setTime(recordatorio.getRec_hora());


			cal_alarm.setTime(dat);
			cal_alarm.set(Calendar.DAY_OF_WEEK, recordatorio.getDia().getDia_dia_semana());

			cal_alarm.set(Calendar.HOUR_OF_DAY, cal_now.get(Calendar.HOUR_OF_DAY));
			//cal_alarm.set(Calendar.AM_PM, cal_now.get(Calendar.AM_PM));
			cal_alarm.set(Calendar.MINUTE, cal_now.get(Calendar.MINUTE));
			cal_alarm.set(Calendar.SECOND, 0);
			cal_alarm.set(Calendar.MILLISECOND, 0);

			if (cal_alarm.getTimeInMillis() < System.currentTimeMillis()) {
				cal_alarm.add(Calendar.DAY_OF_YEAR, 7);
			}
			intent.putExtra("dia", recordatorio.getDia().getDia_nombre());
			intent.putExtra("recordatorio", JsonTransformer.toJson(recordatorio));


			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, recordatorio.getRec_codigo(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

			// pendingIntent.getActivity(getActivity(),0, new Intent(getActivity(), PrincipalActivity.class),0);

			System.out.println("Recordatorio dia " + recordatorio.getDia().getDia_dia_semana() + " Hora " + cal_now.get(Calendar.HOUR_OF_DAY) + " Minuto : " + cal_now.get(Calendar.MINUTE));
			AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
			alarmManager.setInexactRepeating(AlarmManager.RTC, cal_alarm.getTimeInMillis(),  24 * 7 * 60 * 60 * 1000 , pendingIntent);


// notificationId is a unique int for each notification that you must define
      //  Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
      //  v.vibrate(500);

        // Vibrate for 500 milliseconds
		notificationManager.notify(2, mBuilder.build());
		Toast.makeText(context, "Alarm....", Toast.LENGTH_LONG).show();
	}


}
