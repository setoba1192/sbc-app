package com.sbc.util;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.logging.Logger;

public class UnixTimestampDeserializer extends JsonDeserializer<Timestamp> {

    @Override
    public Timestamp deserialize(com.fasterxml.jackson.core.JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String timestamp = jp.getText().trim();

        try {
            return new Timestamp(Long.valueOf(timestamp + "000"));
        } catch (NumberFormatException e) {
           e.printStackTrace();
            return null;
        }
    }
}