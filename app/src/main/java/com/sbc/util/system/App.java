package com.sbc.util.system;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by JACS1 on 21/04/2017.
 */

public class App {


    public static String versionName(Context context) {

        String version = "";

        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);

            version = pInfo.versionName;

            int verCode = pInfo.versionCode;

            System.out.println("Versión: " + version);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }


}