package com.sbc.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by JACS1 on 21/11/2016.
 */

public class Constantes {

    //public static String IP ="http://192.168.0.14:8080/SBC/";
    public static String IP ="http://ingatom.com:8080/SBC/";
    //http://vps178338.vps.ovh.ca:8080

    private static String UPLOAD = "/upload";

    private static String LOGIN ="/app/login";

    private static String INSPECCION= "/app/inspeccion";

    private static String INSPECCIONESlIST = "/app/inspeccion/list";

    private static String REPORTEOCASIONAL = "/app/reporteocasional";

    private static String RECORDATORIO = "/app/recordatorio";


    private static String MY_PREFS_NAME = "SBCPreferences";






    public static String getUsuario(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constantes.MY_PREFS_NAME, Context.MODE_PRIVATE);
        String usuario = prefs.getString("usuario", null);

        return usuario;
    }
    public static void setPreferences(Context context,String login) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constantes.MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString("usuario", login);
        editor.commit();
    }

    public static void setUrl(Context context,String url) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constantes.MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString("url", url);
        editor.commit();
    }

    public static String getUrl(Context context) {

        return IP;
    }

    public static void setSession(Context context,boolean login) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constantes.MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putBoolean("login", login);
        editor.commit();
    }

   public static boolean isSession(Context context){
       SharedPreferences prefs = context.getSharedPreferences(Constantes.MY_PREFS_NAME, Context.MODE_PRIVATE);
       boolean login = prefs.getBoolean("login", false);

       return login;
   }

   public static void clearPreferences(Context context){
       SharedPreferences.Editor editor = context.getSharedPreferences(Constantes.MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
       editor.clear();
       editor.commit();

   }

    public static String getIP(Context context) {
        return getUrl(context)+IP;
    }

    public static String getUPLOAD(Context context) {
        return getUrl(context)+UPLOAD;
    }

    public static String getLOGIN(Context context) {
        return getUrl(context)+LOGIN;
    }

    public static String getINSPECCION(Context context) {
        return getUrl(context)+INSPECCION;
    }

    public static String getINSPECCIONESlIST(Context context) {
        return getUrl(context)+INSPECCIONESlIST;
    }

    public static String getREPORTEOCASIONAL(Context context) {
        return getUrl(context)+REPORTEOCASIONAL;
    }

    public static String getRECORDATORIO(Context context) {
        return getUrl(context)+RECORDATORIO;
    }
}
