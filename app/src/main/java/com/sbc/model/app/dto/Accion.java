package com.sbc.model.app.dto;

/**
 * Created by Luna on 26/01/2018.
 */

public enum Accion {

    VISIBLE,INSPECCIONAR,REFORZAR
}
