package com.sbc.model.app;

public class Persona {

	private int per_codigo;

	private String per_identificacion;

	private String per_nombre;

	private String per_apellido;

	private String per_telefono;

	private String per_email;

	private boolean per_estado;

	private boolean asignacion;

	public boolean isPer_estado() {
		return per_estado;
	}

	public void setPer_estado(boolean per_estado) {
		this.per_estado = per_estado;
	}

	public boolean isAsignacion() {
		return asignacion;
	}

	public void setAsignacion(boolean asignacion) {
		this.asignacion = asignacion;
	}

	public int getPer_codigo() {
		return per_codigo;
	}

	public void setPer_codigo(int per_codigo) {
		this.per_codigo = per_codigo;
	}

	public String getPer_identificacion() {
		return per_identificacion;
	}

	public void setPer_identificacion(String per_identificacion) {
		this.per_identificacion = per_identificacion;
	}

	public String getPer_nombre() {
		return per_nombre;
	}

	public void setPer_nombre(String per_nombre) {
		this.per_nombre = per_nombre;
	}

	public String getPer_apellido() {
		return per_apellido;
	}

	public void setPer_apellido(String per_apellido) {
		this.per_apellido = per_apellido;
	}

	public String getPer_telefono() {
		return per_telefono;
	}

	public void setPer_telefono(String per_telefono) {
		this.per_telefono = per_telefono;
	}

	public String getPer_email() {
		return per_email;
	}

	public void setPer_email(String per_email) {
		this.per_email = per_email;
	}

}
