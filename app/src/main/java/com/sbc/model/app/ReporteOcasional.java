package com.sbc.model.app;

public class ReporteOcasional {

	private int reo_codigo;

	private String reo_area;

	private String reo_observacion;

	public int getReo_codigo() {
		return reo_codigo;
	}

	public void setReo_codigo(int reo_codigo) {
		this.reo_codigo = reo_codigo;
	}

	public String getReo_area() {
		return reo_area;
	}

	public void setReo_area(String reo_area) {
		this.reo_area = reo_area;
	}

	public String getReo_observacion() {
		return reo_observacion;
	}

	public void setReo_observacion(String reo_observacion) {
		this.reo_observacion = reo_observacion;
	}

}
