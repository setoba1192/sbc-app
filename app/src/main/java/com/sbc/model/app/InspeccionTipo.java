package com.sbc.model.app;

public class InspeccionTipo {

	private int int_codigo;

	private String int_nombre;

	public int getInt_codigo() {
		return int_codigo;
	}

	public void setInt_codigo(int int_codigo) {
		this.int_codigo = int_codigo;
	}

	public String getInt_nombre() {
		return int_nombre;
	}

	public void setInt_nombre(String int_nombre) {
		this.int_nombre = int_nombre;
	}
	
	

}
