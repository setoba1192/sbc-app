package com.sbc.model.app;

/**
 * Created by Luna on 27/01/2018.
 */

public class ProcedimientoEstado {

    private String nombre;
    private int codigo;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
}
