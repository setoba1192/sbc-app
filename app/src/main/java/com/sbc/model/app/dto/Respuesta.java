package com.sbc.model.app.dto;

public class Respuesta {

    private String mensaje;

    private Object data;

    private Type type;

    private boolean login;

    public Respuesta(String mensaje, Type type) {

        this.mensaje = mensaje;

        this.type = type;
    }

    public Respuesta() {

    }

    public boolean isLogin() {
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public enum Type {

        ERROR, SUCCESS, WARNING

    }

}
