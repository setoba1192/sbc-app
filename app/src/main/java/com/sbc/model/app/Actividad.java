package com.sbc.model.app;

import java.sql.Timestamp;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_NULL)
public class Actividad {

	private int act_codigo;

	private String act_nombre;

	private String act_descripcion;

	private boolean act_estado;

	private Timestamp act_fecha;

	private List<Procedimiento> procedimientos;

	private int procedimientosCount;

	public int getProcedimientosCount() {
		return procedimientosCount;
	}

	public void setProcedimientosCount(int procedimientosCount) {
		this.procedimientosCount = procedimientosCount;
	}

	public List<Procedimiento> getProcedimientos() {
		return procedimientos;
	}

	public void setProcedimientos(List<Procedimiento> procedimientos) {
		this.procedimientos = procedimientos;
	}

	public String getAct_descripcion() {
		return act_descripcion;
	}

	public void setAct_descripcion(String act_descripcion) {
		this.act_descripcion = act_descripcion;
	}

	public boolean isAct_estado() {
		return act_estado;
	}

	public void setAct_estado(boolean act_estado) {
		this.act_estado = act_estado;
	}

	public Timestamp getAct_fecha() {
		return act_fecha;
	}

	public void setAct_fecha(Timestamp act_fecha) {
		this.act_fecha = act_fecha;
	}

	public int getAct_codigo() {
		return act_codigo;
	}

	public void setAct_codigo(int act_codigo) {
		this.act_codigo = act_codigo;
	}

	public String getAct_nombre() {
		return act_nombre;
	}

	public void setAct_nombre(String act_nombre) {
		this.act_nombre = act_nombre;
	}

}
