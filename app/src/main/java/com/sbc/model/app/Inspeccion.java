package com.sbc.model.app;

import java.sql.Timestamp;
import java.util.List;

public class Inspeccion {

	private long ins_codigo;

	private Timestamp ins_fecha;

	private Actividad actividad;

	private Area area;

	private Persona persona;

	private String ins_observacion;

	private boolean ins_estado;

	private InspeccionTipo inspeccionTipo;

	private List<InspeccionDetalle> inspeccionDetalles;

	private Refuerzo refuerzo;

	public Refuerzo getRefuerzo() {
		return refuerzo;
	}

	public void setRefuerzo(Refuerzo refuerzo) {
		this.refuerzo = refuerzo;
	}

	public List<InspeccionDetalle> getInspeccionDetalles() {
		return inspeccionDetalles;
	}

	public void setInspeccionDetalles(List<InspeccionDetalle> inspeccionDetalles) {
		this.inspeccionDetalles = inspeccionDetalles;
	}

	public InspeccionTipo getInspeccionTipo() {
		return inspeccionTipo;
	}

	public void setInspeccionTipo(InspeccionTipo inspeccionTipo) {
		this.inspeccionTipo = inspeccionTipo;
	}

	public long getIns_codigo() {
		return ins_codigo;
	}

	public void setIns_codigo(long ins_codigo) {
		this.ins_codigo = ins_codigo;
	}

	public Timestamp getIns_fecha() {
		return ins_fecha;
	}

	public void setIns_fecha(Timestamp ins_fecha) {
		this.ins_fecha = ins_fecha;
	}

	public Actividad getActividad() {
		return actividad;
	}

	public void setActividad(Actividad actividad) {
		this.actividad = actividad;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public String getIns_observacion() {
		return ins_observacion;
	}

	public void setIns_observacion(String ins_observacion) {
		this.ins_observacion = ins_observacion;
	}

	public boolean isIns_estado() {
		return ins_estado;
	}

	public void setIns_estado(boolean ins_estado) {
		this.ins_estado = ins_estado;
	}

}
