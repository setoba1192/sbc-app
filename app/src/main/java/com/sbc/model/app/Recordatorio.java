package com.sbc.model.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sbc.util.UnixTimestampDeserializer;


import java.sql.Timestamp;
import java.util.Date;

public class Recordatorio {

	private int rec_codigo;

	private Dia dia;


	private Timestamp rec_hora;

	private Area area;

	private Persona persona;

	private boolean activo;

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public int getRec_codigo() {
		return rec_codigo;
	}

	public void setRec_codigo(int rec_codigo) {
		this.rec_codigo = rec_codigo;
	}

	public Dia getDia() {
		return dia;
	}

	public void setDia(Dia dia) {
		this.dia = dia;
	}

	public Timestamp getRec_hora() {
		return rec_hora;
	}

	public void setRec_hora(Timestamp rec_hora) {
		this.rec_hora = rec_hora;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}
}
