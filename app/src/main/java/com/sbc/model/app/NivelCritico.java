package com.sbc.model.app;

public class NivelCritico {

	private int nic_codigo;

	private String nic_nombre;

	private int nic_valor;

	public int getNic_codigo() {
		return nic_codigo;
	}

	public void setNic_codigo(int nic_codigo) {
		this.nic_codigo = nic_codigo;
	}

	public String getNic_nombre() {
		return nic_nombre;
	}

	public void setNic_nombre(String nic_nombre) {
		this.nic_nombre = nic_nombre;
	}

	public int getNic_valor() {
		return nic_valor;
	}

	public void setNic_valor(int nic_valor) {
		this.nic_valor = nic_valor;
	}

}
