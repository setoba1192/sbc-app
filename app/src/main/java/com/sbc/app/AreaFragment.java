package com.sbc.app;

import android.app.AlarmManager;
import android.app.DatePickerDialog;

import android.app.PendingIntent;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.Response;


import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.daimajia.slider.library.SliderLayout;
import com.google.gson.Gson;
import com.philliphsu.bottomsheetpickers.BottomSheetPickerDialog;
import com.philliphsu.bottomsheetpickers.time.BottomSheetTimePickerDialog;
import com.philliphsu.bottomsheetpickers.time.grid.GridTimePickerDialog;

import com.sbc.model.app.Dia;
import com.sbc.model.app.PersonaArea;
import com.sbc.model.app.Recordatorio;
import com.sbc.model.app.dto.Respuesta;
import com.sbc.movil.AppController;
import com.sbc.movil.R;
import com.sbc.movil.app.ActividadesActivity;

import com.sbc.movil.app.EmpresasActivity;
import com.sbc.movil.app.LoginActivity;
import com.sbc.movil.app.PrincipalActivity;
import com.sbc.util.Constantes;
import com.sbc.util.JsonTransformer;
import com.sbc.util.MyBroadcastReceiver;
import com.shashank.sony.fancygifdialoglib.FancyGifDialog;
import com.shashank.sony.fancygifdialoglib.FancyGifDialogListener;


import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.hypertrack.smart_scheduler.Job;
import io.hypertrack.smart_scheduler.SmartScheduler;

public class AreaFragment extends Fragment implements
        BottomSheetTimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener, SmartScheduler.JobScheduledCallback  {


    private SliderLayout imagesSlider;

    private SweetAlertDialog pDialog;

    private String dias[] = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado", "Domingo"};


    private List<Recordatorio> recordatorios;

    private Recordatorio recordatorioSeleccionado;

    private PersonaArea personaArea;

    private int positionSelected;

    private Button buttonSelected;

    private TextView textAreaNombre;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.app_area_fragment, container, false);

        personaArea = JsonTransformer.fromJSON(Constantes.getUsuario(getActivity()), PersonaArea.class);

        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        TextView textTitulo = (TextView) toolbar.findViewById(R.id.textTitulo);
        textTitulo.setText(personaArea.getArea().getAre_nombre());


        ImageButton btnCerrarSesion = v.findViewById(R.id.btnCerrarSesion);
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cerrarSesion();

            }
        });

        textAreaNombre = (TextView)v.findViewById(R.id.textAreaNombre);
        textAreaNombre.setText(personaArea.getArea().getAre_nombre());

        float widthheight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics());
        float margin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());

        LinearLayout layoutDiasArea = (LinearLayout) v.findViewById(R.id.linearDiasArea);

        Calendar now = Calendar.getInstance();

        GridTimePickerDialog grid = new GridTimePickerDialog.Builder(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                DateFormat.is24HourFormat(getContext()))
                /* ... Set additional options ... */
                .build();
        for (Dia dia : personaArea.getArea().getDias()) {


            Button button = new Button(getContext());
            button.setText(dia.getDia_nombre().substring(0, 1));
            button.setTypeface(Typeface.DEFAULT_BOLD);
            button.setBackgroundResource(dia.isLaboral() ? R.drawable.circle_shape_active : R.drawable.circle_shape);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Math.round(widthheight), Math.round(widthheight));
            params.setMargins(Math.round(margin), Math.round(margin), Math.round(margin), Math.round(margin));

            params.gravity = Gravity.CENTER;

            button.setLayoutParams(params);
            button.setPadding(25, 10, 25, 10);


            layoutDiasArea.addView(button);
        }


        recordatorios = obtenerLista();
        LinearLayout layoutDias = (LinearLayout) v.findViewById(R.id.layoutDias);

        int counter = 0;
        for (final Recordatorio recordatorio : recordatorios) {


            final Button button = new Button(getContext());

            button.setText(recordatorio.getDia().getDia_nombre().substring(0, 1));
            button.setTypeface(Typeface.DEFAULT_BOLD);
            button.setBackgroundResource(recordatorio.isActivo() ? R.drawable.circle_shape_active : R.drawable.circle_shape);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Math.round(widthheight), Math.round(widthheight));
            params.setMargins(Math.round(margin), Math.round(margin), Math.round(margin), Math.round(margin));

            params.gravity = Gravity.CENTER;

            button.setLayoutParams(params);
            button.setPadding(25, 10, 25, 10);

            final int finalCounter = counter;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    recordatorioSeleccionado = recordatorio;
                    buttonSelected = button;
                    System.out.println("seleccionar!! " + recordatorio.getRec_codigo());

                    if (recordatorio.getRec_codigo() != 0) {
                        dialogRecordatorio(recordatorio, finalCounter);
                    } else {
                        DialogFragment dialog = createDialog(1, finalCounter);
                        dialog.show(getFragmentManager(), "");

                    }
                }
            });


            layoutDias.addView(button);
            counter++;
        }

        //TextView textPrincipal = (TextView) v.findViewById(R.id.textPrincipal);


        return v;
    }

    public static AreaFragment newInstance(String text) {

        AreaFragment areaFragment = new AreaFragment();

        return areaFragment;
    }

    private List<Recordatorio> obtenerLista() {

        List<Recordatorio> recordatorios = new ArrayList<>();

        for (int i = 0; i < 7; i++) {

            Dia dia = new Dia();
            dia.setDia_codigo(i + 1);
            dia.setDia_nombre(dias[i]);
            Recordatorio recordatorio = new Recordatorio();
            recordatorio.setDia(dia);
            recordatorio.setActivo(false);

            recordatorio = buscarRecordatorio(recordatorio);
            recordatorios.add(recordatorio);


        }

        return recordatorios;

    }

    private Recordatorio buscarRecordatorio(Recordatorio recordatorio) {

        for (Recordatorio record : personaArea.getRecordatorios()) {

            if (recordatorio.getDia().getDia_codigo() == record.getDia().getDia_codigo()) {

                return record;
            }

        }

        return recordatorio;
    }

    public void verRecordatorios(){

        System.out.println("Ver recordatorios!  tamano"+this.personaArea.getRecordatorios().size());

        int count = 0;
        for (Recordatorio record : personaArea.getRecordatorios()) {

            System.out.print("Recordatorio !!!!!!!! Revisar "+count+++" : "+JsonTransformer.toJson(record));
        }

    }


    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

    }

    @Override
    public void onTimeSet(ViewGroup viewGroup, int hourOfDay, int minute) {

        Calendar cal = new java.util.GregorianCalendar();

        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
        cal.getTimeInMillis();
        System.out.println("Time set: " + DateFormat.getTimeFormat(getContext()).format(cal.getTime()));

        recordatorioSeleccionado.setRec_hora(new Timestamp(cal.getTimeInMillis()));


        try {
            registrarRecordatorio();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private DialogFragment createDialog(int checkedId, int positionSelected) {

        this.positionSelected = positionSelected;
        return createDialogWithBuilders(checkedId);

    }

    private DialogFragment createDialogWithBuilders(int checkedId) {
        BottomSheetPickerDialog.Builder builder = null;
        boolean custom = false;
        boolean customDark = false;
        boolean themeDark = false;


        Calendar now = Calendar.getInstance();
        if(recordatorioSeleccionado.getRec_hora()!=null) {
            now.setTime(recordatorioSeleccionado.getRec_hora());
        }
        builder = new GridTimePickerDialog.Builder(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                DateFormat.is24HourFormat(getContext()));
        GridTimePickerDialog.Builder gridDialogBuilder =
                (GridTimePickerDialog.Builder) builder;
        if (custom || customDark) {
            gridDialogBuilder.setHeaderTextColorSelected(0xFFFF4081)
                    .setHeaderTextColorUnselected(0x4AFF4081)
                    .setTimeSeparatorColor(0xFF000000)
                    .setHalfDayButtonColorSelected(0xFFFF4081)
                    .setHalfDayButtonColorUnselected(0x4AFF4081);
        }


        return builder.build();
    }

    private void registrarRecordatorio() throws JSONException {

        System.out.println("Recordatorio enviado " + JsonTransformer.toJson(recordatorioSeleccionado));

        recordatorioSeleccionado.setArea(personaArea.getArea());
        recordatorioSeleccionado.setPersona(personaArea.getPersona());

        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#373447"));
        pDialog.setTitleText("Registrando recordatorio...");
        pDialog.setCancelable(false);
        pDialog.show();


        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, Constantes.getRECORDATORIO(getActivity()), new JSONObject(JsonTransformer.toJson(recordatorioSeleccionado)),
                createMyReqSuccessListener(),   createMyReqErrorListener());

        //TODO: handle success



        AppController.getInstance().addToRequestQueue(jsonRequest, "NN");

    }

    private Response.Listener<JSONObject> createMyReqSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                System.out.println(response.toString());
                final Respuesta respuesta = JsonTransformer.fromJSON(response.toString(), Respuesta.class);

                int type = SweetAlertDialog.NORMAL_TYPE;

                if (respuesta.getType() == Respuesta.Type.ERROR) {
                    type = SweetAlertDialog.ERROR_TYPE;
                } else if (respuesta.getType() == Respuesta.Type.WARNING) {

                    type = SweetAlertDialog.WARNING_TYPE;
                } else {
                    type = SweetAlertDialog.SUCCESS_TYPE;
                }

                if (respuesta.getType() == Respuesta.Type.SUCCESS) {

                    String recordatorioJson = JsonTransformer.toJson(respuesta.getData());
                    System.out.println("RecordatorioJson " + recordatorioJson);
                    recordatorioSeleccionado = JsonTransformer.fromJSON(recordatorioJson, Recordatorio.class);
                    recordatorios.get(positionSelected).setRec_codigo(recordatorioSeleccionado.getRec_codigo());
                    recordatorios.get(positionSelected).setActivo(recordatorioSeleccionado.isActivo());
                    System.out.println("RecordatorioSeleccionado " + recordatorioSeleccionado.getRec_codigo());
                    buttonSelected.setBackgroundResource(recordatorioSeleccionado.isActivo() ? R.drawable.circle_shape_active : R.drawable.circle_shape);

                    actualizarAlarm(recordatorioSeleccionado);
                }

                new SweetAlertDialog(getContext(), type)
                        .setTitleText("Información")
                        .setContentText(respuesta.getMensaje())
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismiss();


                            }
                        })
                        .show();
            }
        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                pDialog.dismiss();
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText("Ocurrió un error durante el proceso, intentelo de nuevo...")
                        .show();
            }
        };
    }

    public void dialogRecordatorio(Recordatorio recordatorio, final int positionSelected) {

        String textoBotton = recordatorio.isActivo() ? "Deshabilitar" : "Habilitar";

        String hora = new SimpleDateFormat("hh:mm a").format(recordatorio.getRec_hora());

        String color = recordatorio.isActivo() ? "#EB5E28" : "#FFA9A7A8";

        new FancyGifDialog.Builder(getActivity())
                .setTitle("Recordatorio")
                .setMessage("Recordatorio establecido los " + recordatorio.getDia().getDia_nombre() + " a las : " + hora)
                .setNegativeBtnText(textoBotton)
                .setPositiveBtnBackground("#EB5E28")
                .setPositiveBtnText("Modificar")
                .setNegativeBtnBackground(color)
                .setGifResource(R.drawable.alarma)   //Pass your Gif here
                .isCancellable(true)
                .OnPositiveClicked(new FancyGifDialogListener() {
                    @Override
                    public void OnClick() {

                        DialogFragment dialog = createDialog(1, positionSelected);
                        dialog.show(getFragmentManager(), "");
                    }
                })
                .OnNegativeClicked(new FancyGifDialogListener() {
                    @Override
                    public void OnClick() {
                        recordatorioSeleccionado.setActivo(!recordatorioSeleccionado.isActivo());
                        try {
                            cambiarEstado();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .build();

    }

    public void cambiarEstado() throws JSONException {

        recordatorioSeleccionado.setArea(personaArea.getArea());
        recordatorioSeleccionado.setPersona(personaArea.getPersona());
        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#373447"));
        pDialog.setTitleText("Registrando recordatorio...");
        pDialog.setCancelable(false);
        pDialog.show();

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, Constantes.getRECORDATORIO(getActivity()),new JSONObject(JsonTransformer.toJson(recordatorioSeleccionado)),
                success2(), error2());
        AppController.getInstance().addToRequestQueue(jsonRequest, "NN");
    }

    private Response.Listener<JSONObject> success2() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                System.out.println(response.toString());
                final Respuesta respuesta = JsonTransformer.fromJSON(response.toString(), Respuesta.class);

                int type = SweetAlertDialog.NORMAL_TYPE;

                if (respuesta.getType() == Respuesta.Type.ERROR) {
                    type = SweetAlertDialog.ERROR_TYPE;
                } else if (respuesta.getType() == Respuesta.Type.WARNING) {

                    type = SweetAlertDialog.WARNING_TYPE;
                } else {
                    type = SweetAlertDialog.SUCCESS_TYPE;
                }

                if (respuesta.getType() == Respuesta.Type.SUCCESS) {

                    String recordatorioJson = JsonTransformer.toJson(respuesta.getData());
                    System.out.println("RecordatorioJson " + recordatorioJson);
                    recordatorioSeleccionado = JsonTransformer.fromJSON(recordatorioJson, Recordatorio.class);
                    recordatorios.get(positionSelected).setRec_codigo(recordatorioSeleccionado.getRec_codigo());
                    System.out.println("RecordatorioSeleccionado " + recordatorioSeleccionado.getRec_codigo());


                    buttonSelected.setBackgroundResource(recordatorioSeleccionado.isActivo() ? R.drawable.circle_shape_active : R.drawable.circle_shape);

                    actualizarAlarm(recordatorioSeleccionado);
                    String habilitado = recordatorioSeleccionado.isActivo() ? "habilitado" : "deshabilitado";
                    respuesta.setMensaje("Recordatorio " + habilitado + " exitosamente");
                }

                new SweetAlertDialog(getContext(), type)
                        .setTitleText("Información")
                        .setContentText(respuesta.getMensaje())
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismiss();


                            }
                        })
                        .show();

            }
        };
    }


    private Response.ErrorListener error2() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //TODO: handle failure
                pDialog.dismiss();
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")

                        .setContentText("Ocurrió un error durante el proceso, intentelo de nuevo...")
                        .show();
            }
        };
    }

    public void cerrarSesion() {

        new FancyGifDialog.Builder(getActivity())
                .setTitle("Cerrar Sesión")
                .setMessage("¿Desea cerrar sesión?.")
                .setNegativeBtnText("Cancelar")
                .setPositiveBtnBackground("#EB5E28")
                .setPositiveBtnText("Aceptar")
                .setNegativeBtnBackground("#FFA9A7A8")
                .setGifResource(R.drawable.inspect)   //Pass your Gif here
                .isCancellable(true)
                .OnPositiveClicked(new FancyGifDialogListener() {
                    @Override
                    public void OnClick() {

                        Constantes.clearPreferences(getActivity());
                        // Toast.makeText(getActivity(), "Ok", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), MyBroadcastReceiver.class);
                        for (Recordatorio recordatorio : personaArea.getRecordatorios()) {
                            PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), recordatorio.getRec_codigo(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                            AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                            alarmManager.cancel(pendingIntent);
                        }
                        Constantes.setSession(getActivity(), false);
                        Intent i = new Intent(getActivity(), LoginActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    }
                })
                .OnNegativeClicked(new FancyGifDialogListener() {
                    @Override
                    public void OnClick() {
                        //    Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_SHORT).show();
                    }
                })
                .build();

    }

    public void actualizarAlarm(Recordatorio recordatorio) {


        Intent intent = new Intent(getActivity(), MyBroadcastReceiver.class);
        if (recordatorio.isActivo()) {

            Date dat = new Date();

            Calendar cal_alarm = Calendar.getInstance();
            Calendar cal_now = Calendar.getInstance();
            cal_now.setTime(recordatorio.getRec_hora());


            cal_alarm.setTime(dat);
            cal_alarm.set(Calendar.DAY_OF_WEEK, recordatorio.getDia().getDia_dia_semana());

            cal_alarm.set(Calendar.HOUR_OF_DAY, cal_now.get(Calendar.HOUR_OF_DAY));
            //cal_alarm.set(Calendar.AM_PM, cal_now.get(Calendar.AM_PM));
            cal_alarm.set(Calendar.MINUTE, cal_now.get(Calendar.MINUTE));
            cal_alarm.set(Calendar.SECOND, 0);
            cal_alarm.set(Calendar.MILLISECOND, 0);

            if (cal_alarm.getTimeInMillis() < System.currentTimeMillis()) {
                cal_alarm.add(Calendar.DAY_OF_YEAR, 7);
            }
            intent.putExtra("dia", recordatorio.getDia().getDia_nombre());
            intent.putExtra("recordatorio", JsonTransformer.toJson(recordatorio));


            PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity().getApplicationContext(), recordatorio.getRec_codigo(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

           // pendingIntent.getActivity(getActivity(),0, new Intent(getActivity(), PrincipalActivity.class),0);

            System.out.println("Recordatorio dia " + recordatorio.getDia().getDia_dia_semana() + " Hora " + cal_now.get(Calendar.HOUR_OF_DAY) + " Minuto : " + cal_now.get(Calendar.MINUTE));
            AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(getActivity().ALARM_SERVICE);
           // alarmManager.setInexactRepeating(AlarmManager.RTC, cal_alarm.getTimeInMillis(), 1 * 60 * 60 * 1000, pendingIntent);
            alarmManager.setInexactRepeating(AlarmManager.RTC, cal_alarm.getTimeInMillis(),  24 * 7 * 60 * 60 * 1000 , pendingIntent);




        }else{


            PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), recordatorio.getRec_codigo(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

            AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);


        }
        personaArea.setRecordatorios(this.recordatorios);
        Constantes.setPreferences(getActivity(), JsonTransformer.toJson(personaArea));
        verRecordatorios();

    }

    @Override
    public void onJobScheduled(Context context,final Job job) {
        if (job != null) {

                    Toast.makeText(getActivity(), "Job: " + job.getJobId() + " scheduled!", Toast.LENGTH_SHORT).show();
                }



    }


}