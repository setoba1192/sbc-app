package com.sbc.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sbc.adapter.app.ActividadAdapter;
import com.sbc.adapter.app.InspeccionAdapter;
import com.sbc.model.app.Actividad;
import com.sbc.model.app.Inspeccion;
import com.sbc.movil.R;
import com.sbc.movil.app.ActividadesActivity;
import com.shashank.sony.fancygifdialoglib.FancyGifDialog;
import com.shashank.sony.fancygifdialoglib.FancyGifDialogListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;

public class InspeccionesFragment extends Fragment {


    private InspeccionAdapter inspeccionAdapter;

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ProgressDialog pDialog;
    private FancyButton btnAddInspection;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.app_inspecciones_fragment, container, false);

        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);

        AppCompatActivity activity = (AppCompatActivity) getActivity();


        RelativeLayout relativeLayout = (RelativeLayout) v.findViewById(R.id.parentLayout);

        btnAddInspection = (FancyButton) v.findViewById(R.id.btn_add_inspection);

        btnAddInspection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog();
            }
        });


        TextView textTitulo = (TextView) toolbar.findViewById(R.id.textTitulo);
        textTitulo.setText("Inspecciones");

        activity.setSupportActionBar(toolbar);


        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);

        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        inspeccionAdapter = new InspeccionAdapter(cargarInspecciones(), getActivity());
        mRecyclerView.setAdapter(inspeccionAdapter);

        return v;
    }

    public List<Inspeccion> cargarInspecciones() {

        List<Inspeccion> inspeccions = new ArrayList<>();


        for (int i = 0; i < 3; i++) {
            //int randomNum = 3 + (int)(Math.random() * ((10 - 3) + 1));

            Inspeccion inspeccion = new Inspeccion();
           // inspeccion.setFecha(new Date());

            Actividad actividad = new Actividad();
           // actividad.setNombre("Actividad 0" + i);

            inspeccion.setActividad(actividad);

            inspeccions.add(inspeccion);
        }


        return inspeccions;
    }

    public static InspeccionesFragment newInstance(String text) {

        InspeccionesFragment actividadesFragment = new InspeccionesFragment();

        return actividadesFragment;
    }


    public void dialog() {

        new FancyGifDialog.Builder(getActivity())
                .setTitle("Inicio de inspección")
                .setMessage("Presione aceptar para iniciar la inspección. Una vez presióne aceptar, deberá seleccionar una actividad.")
                .setNegativeBtnText("Cancelar")
                .setPositiveBtnBackground("#EB5E28")
                .setPositiveBtnText("Aceptar")
                .setNegativeBtnBackground("#FFA9A7A8")
                .setGifResource(R.drawable.inspect)   //Pass your Gif here
                .isCancellable(true)
                .OnPositiveClicked(new FancyGifDialogListener() {
                    @Override
                    public void OnClick() {
                       // Toast.makeText(getActivity(), "Ok", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(getActivity(), ActividadesActivity.class);
                        startActivity(i);
                    }
                })
                .OnNegativeClicked(new FancyGifDialogListener() {
                    @Override
                    public void OnClick() {
                    //    Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_SHORT).show();
                    }
                })
                .build();

    }


}