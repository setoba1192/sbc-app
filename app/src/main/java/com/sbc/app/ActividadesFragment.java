package com.sbc.app;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.google.gson.Gson;
import com.sbc.adapter.app.ActividadAdapter;
import com.sbc.model.app.Actividad;
import com.sbc.model.app.PersonaArea;
import com.sbc.model.app.dto.Accion;
import com.sbc.movil.R;
import com.sbc.util.Constantes;
import com.sbc.util.JsonTransformer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class ActividadesFragment extends Fragment {


    private ActividadAdapter actividadAdapter;

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ProgressDialog pDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.app_actividades_fragment, container, false);

        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);

        AppCompatActivity activity = (AppCompatActivity) getActivity();


        RelativeLayout relativeLayout = (RelativeLayout)v.findViewById(R.id.parentLayout);



        PersonaArea personaArea = JsonTransformer.fromJSON(Constantes.getUsuario(getActivity()),PersonaArea.class);


        TextView textTitulo = (TextView)toolbar.findViewById(R.id.textTitulo);
        textTitulo.setText("Tareas Críticas");

        activity.setSupportActionBar(toolbar);


        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);

        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        actividadAdapter = new ActividadAdapter(personaArea.getActividades(), getActivity(), Accion.VISIBLE);
        mRecyclerView.setAdapter(actividadAdapter);

        return v;
    }

    public List<Actividad> cargarActividades(){

        List<Actividad> actividades = new ArrayList<>();


        for(int i = 0; i<10;i++){
            int randomNum = 3 + (int)(Math.random() * ((10 - 3) + 1));

            Actividad actividad = new Actividad();
            actividad.setAct_nombre("Actividad 0"+(i+1));
            //actividad.setProcedimientos(randomNum);

            actividades.add(actividad);
        }


        return actividades;
    }

    public static ActividadesFragment newInstance(String text) {

        ActividadesFragment actividadesFragment = new ActividadesFragment();

        return actividadesFragment;
    }




}