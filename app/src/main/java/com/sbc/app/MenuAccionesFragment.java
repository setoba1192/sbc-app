package com.sbc.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.sbc.movil.R;
import com.sbc.movil.app.InspeccionesActivity;

import com.sbc.movil.app.ReporteOcasionalActivity;

import mehdi.sakout.fancybuttons.FancyButton;

public class MenuAccionesFragment extends Fragment {


    private SliderLayout imagesSlider;

    private ProgressDialog pDialog;

    private FancyButton btnInspecciones;
    private FancyButton btnReporteOcasional;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.app_menu_acciones_fragment, container, false);

        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        TextView textTitulo = (TextView) toolbar.findViewById(R.id.textTitulo);
        textTitulo.setText("Acciónes");

        btnInspecciones =(FancyButton) v.findViewById(R.id.btn_add_inspection);
        btnInspecciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getContext(), InspeccionesActivity.class);
                startActivity(i);
            }
        });

        btnReporteOcasional = (FancyButton) v.findViewById(R.id.btn_add_reporte);
        btnReporteOcasional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getContext(), ReporteOcasionalActivity.class);
                startActivity(i);
            }
        });



        //TextView textPrincipal = (TextView) v.findViewById(R.id.textPrincipal);


        return v;
    }

    public static MenuAccionesFragment newInstance(String text) {

        MenuAccionesFragment areaFragment = new MenuAccionesFragment();

        return areaFragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


    }
}