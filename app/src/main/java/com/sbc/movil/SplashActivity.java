package com.sbc.movil;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;

import com.sbc.movil.app.EmpresasActivity;
import com.sbc.movil.app.LoginActivity;
import com.sbc.movil.app.PrincipalActivity;
import com.sbc.util.Constantes;
import com.sbc.util.Util;

import java.io.File;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        Intent intent = null;

        if(Constantes.isSession(this)){

            intent = new Intent(this, PrincipalActivity.class);
        }else{
            intent = new Intent(this, LoginActivity.class);
        }

        startActivity(intent);
        finish();
    }
}