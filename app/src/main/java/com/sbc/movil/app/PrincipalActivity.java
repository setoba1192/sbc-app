package com.sbc.movil.app;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationItem;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationView;
import com.luseen.luseenbottomnavigation.BottomNavigation.OnBottomNavigationItemClickListener;
import com.sbc.app.ActividadesFragment;
import com.sbc.app.AreaFragment;

import com.sbc.app.MenuAccionesFragment;
import com.sbc.movil.R;
import com.sbc.util.Util;

import java.io.File;


/**
 * Created by GIGAMOLE on 28.03.2016.
 */
public class PrincipalActivity extends AppCompatActivity {

    // ID to identify permission request
    private static final int MY_PERMISSION_REQUEST_ID = 1;


    private ViewPager viewPager;
    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horizontal_ntb);
        initUI();
    }


    private void initUI() {

        Util.requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "SBCRecorders");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            // Do something on success
        } else {
            // Do something else on failure
        }

        viewPager = (ViewPager) findViewById(R.id.vp_horizontal_ntb);
        viewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        viewPager.setOffscreenPageLimit(5);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                System.out.println("Posicion "+position);

            }

            @Override
            public void onPageSelected(int position) {
                bottomNavigationView.selectTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_bar);

        int[] image = {R.drawable.ic_home, R.drawable.ic_question,
                R.drawable.ic_about_us, R.mipmap.calendar,R.drawable.ic_sad};
        int[] color = {ContextCompat.getColor(this, R.color.colorPrimary), ContextCompat.getColor(this, R.color.colorPrimary),
                ContextCompat.getColor(this, R.color.colorPrimary), ContextCompat.getColor(this, R.color.colorPrimary)};

        if (bottomNavigationView != null) {
            bottomNavigationView.isWithText(true);
            // bottomNavigationView.activateTabletMode();
            //   bottomNavigationView.isColoredBackground(true);
            bottomNavigationView.setTextActiveSize(getResources().getDimension(R.dimen.textDefault));

            bottomNavigationView.setTextInactiveSize(getResources().getDimension(R.dimen.textDefaultUnselect));
            bottomNavigationView.setItemActiveColorWithoutColoredBackground(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            //   bottomNavigationView.setFont(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Noh_normal.ttf"));
        }

        BottomNavigationItem bottomNavigationItem = new BottomNavigationItem
                ("Mi Área", color[0], image[0]);
        BottomNavigationItem bottomNavigationItem1 = new BottomNavigationItem
                ("Tareas C.", color[1], image[3]);
        BottomNavigationItem bottomNavigationItem2 = new BottomNavigationItem
                ("Acciones", color[2], image[1]);
        BottomNavigationItem bottomNavigationItem3 = new BottomNavigationItem
                ("Eventos C.", color[3], image[4]);
        BottomNavigationItem bottomNavigationItem4 = new BottomNavigationItem
                ("Nosotros", color[3], image[2]);

        bottomNavigationView.addTab(bottomNavigationItem);
        bottomNavigationView.addTab(bottomNavigationItem1);
        bottomNavigationView.addTab(bottomNavigationItem2);
       // bottomNavigationView.addTab(bottomNavigationItem3);
       // bottomNavigationView.addTab(bottomNavigationItem4);


        bottomNavigationView.setOnBottomNavigationItemClickListener(new OnBottomNavigationItemClickListener() {
            @Override
            public void onNavigationItemClick(final int index) {
                switch (index) {
                    case 0:
                        cambiarPosicion(index);
                        break;
                    case 1:
                        cambiarPosicion(index);
                        break;
                    case 2:
                        cambiarPosicion(index);
                        break;

                 /*   case 3:
                        cambiarPosicion(index);
                        break;
                    case 4:
                       cambiarPosicion(index);
                        break;*/
                }
            }
        });


        bottomNavigationView.selectTab(0);


    }

    public void cambiarPosicion(final int posicion){

        viewPager.post(new Runnable() {
            @Override
            public void run() {
                viewPager.setCurrentItem(posicion);
            }
        });
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {

            super(fm);
        }




        @Override
        public Fragment getItem(int pos) {
            switch (pos) {

                case 0:


                    return AreaFragment.newInstance("FirstFragment, Instance 1");
                case 1:

                    return ActividadesFragment.newInstance("SecondFragment, Instance 1");
                case 2:

                    return MenuAccionesFragment.newInstance("ThirdFragment, Instance 1");
                /*case 3:

                    return AreaFragment.newInstance("ThirdFragment, Instance 2");
                case 4:

                    return AreaFragment.newInstance("ThirdFragment, Default");
*/
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_ID: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // granted, start OmRecorder
                }
                return;
            }
        }
    }
}
