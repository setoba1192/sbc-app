package com.sbc.movil.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;

import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonArrayRequest;
import com.android.volley.request.JsonObjectRequest;
import com.google.gson.Gson;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sbc.adapter.app.InspeccionAdapter;
import com.sbc.app.InspeccionesFragment;
import com.sbc.model.app.Actividad;
import com.sbc.model.app.Inspeccion;
import com.sbc.model.app.InspeccionRefuerzoCheckList;
import com.sbc.model.app.Persona;
import com.sbc.model.app.PersonaArea;
import com.sbc.model.app.RefuerzoCheckList;
import com.sbc.model.app.dto.Respuesta;
import com.sbc.movil.AppController;
import com.sbc.movil.R;
import com.sbc.util.Constantes;
import com.sbc.util.JsonTransformer;
import com.shashank.sony.fancygifdialoglib.FancyGifDialog;
import com.shashank.sony.fancygifdialoglib.FancyGifDialogListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import mehdi.sakout.fancybuttons.FancyButton;

public class InspeccionesActivity extends AppCompatActivity {

    private InspeccionAdapter inspeccionAdapter;

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ProgressDialog pDialog;
    private FancyButton btnAddInspection;

    private PersonaArea personaArea;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspecciones);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        AppCompatActivity activity = (AppCompatActivity) this;

        personaArea = JsonTransformer.fromJSON(Constantes.getUsuario(this),PersonaArea.class);

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.parentLayout);

        btnAddInspection = (FancyButton) findViewById(R.id.btn_add_inspection);

        btnAddInspection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog();
            }
        });


        TextView textTitulo = (TextView) toolbar.findViewById(R.id.textTitulo);
        textTitulo.setText("Observaciónes");

        activity.setSupportActionBar(toolbar);


        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        mLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);




        //getInspecciones(personaArea.getPersona());
    }

    public List<Inspeccion> cargarInspecciones() {

        List<Inspeccion> inspeccions = new ArrayList<>();


        for (int i = 0; i < 3; i++) {
            //int randomNum = 3 + (int)(Math.random() * ((10 - 3) + 1));

            Inspeccion inspeccion = new Inspeccion();
            // inspeccion.setFecha(new Date());

            Actividad actividad = new Actividad();
            // actividad.setNombre("Actividad 0" + i);

            inspeccion.setActividad(actividad);

            inspeccions.add(inspeccion);
        }


        return inspeccions;
    }

    public static InspeccionesFragment newInstance(String text) {

        InspeccionesFragment actividadesFragment = new InspeccionesFragment();

        return actividadesFragment;
    }


    public void dialog() {

        new FancyGifDialog.Builder(this)
                .setTitle("Inicio de Observación")
                .setMessage("Presione aceptar para iniciar la observación. Una vez presióne aceptar, deberá seleccionar una Tarea Crítica.")
                .setNegativeBtnText("Cancelar")
                .setPositiveBtnBackground("#EB5E28")
                .setPositiveBtnText("Aceptar")
                .setNegativeBtnBackground("#FFA9A7A8")
                .setGifResource(R.drawable.inspect)   //Pass your Gif here
                .isCancellable(true)
                .OnPositiveClicked(new FancyGifDialogListener() {
                    @Override
                    public void OnClick() {
                        // Toast.makeText(getActivity(), "Ok", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(InspeccionesActivity.this, ActividadesActivity.class);
                        //1 : equivalente a inspeccion
                        i.putExtra("tipo", 1);
                        startActivity(i);
                    }
                })
                .OnNegativeClicked(new FancyGifDialogListener() {
                    @Override
                    public void OnClick() {
                        //    Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_SHORT).show();
                    }
                })
                .build();

    }

    public void getInspecciones(Persona persona) throws JSONException {


        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, Constantes.getINSPECCIONESlIST(this) + "?ins_tipo=1", new JSONObject(JsonTransformer.toJson(persona)), createMyReqSuccessListener(),createMyReqErrorListener());

        // Adding String request to request queue
        AppController.getInstance().addToRequestQueue(jsonRequest, "");

    }

    private Response.Listener<JSONObject> createMyReqSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                System.out.println("Respuesta " + response.toString());

                Respuesta respuesta = JsonTransformer.fromJSON(response.toString(), Respuesta.class);

                List<Inspeccion> inspeccions = JsonTransformer.fromJsonArray(JsonTransformer.toJson(respuesta.getData()), Inspeccion.class);
                inspeccionAdapter = new InspeccionAdapter(inspeccions, InspeccionesActivity.this);
                mRecyclerView.setAdapter(inspeccionAdapter);
            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("error");
                error.printStackTrace();

            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            getInspecciones(personaArea.getPersona());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
