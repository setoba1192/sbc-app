package com.sbc.movil.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.sbc.movil.R;
import com.sbc.util.Constantes;

public class EmpresasActivity extends AppCompatActivity {


    private CardView cardViewClipSalud;

    private CardView cardViewMayra;

    private CardView cardViewFreez;

    private CardView cardViewGreen;

    private CardView cardViewCafe;

    private CardView cardViewUniversidad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresas);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        TextView textTitulo = (TextView) toolbar.findViewById(R.id.textTitulo);
        textTitulo.setText("Seleccione su empresa");


        cardViewUniversidad = (CardView) findViewById(R.id.cardUniversidad);
        cardViewUniversidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                irALogin(R.drawable.uao_logo, Constantes.getIP(EmpresasActivity.this));
            }
        });

        cardViewClipSalud = (CardView) findViewById(R.id.cardClipsalud);
        cardViewClipSalud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                irALogin(R.drawable.clipsalud_logo,  Constantes.getIP(EmpresasActivity.this));
            }
        });

        cardViewMayra = (CardView) findViewById(R.id.cardMayra);
        cardViewMayra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                irALogin(R.drawable.mayra_logo,  Constantes.getIP(EmpresasActivity.this));

            }
        });

        cardViewFreez = (CardView) findViewById(R.id.cardFreez);
        cardViewFreez.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                irALogin(R.drawable.freez_logo,  Constantes.getIP(EmpresasActivity.this));

            }
        });


        cardViewGreen = (CardView) findViewById(R.id.cardGreen);
        cardViewGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                irALogin(R.drawable.green_logo,  Constantes.getIP(EmpresasActivity.this));


            }
        });


        cardViewCafe = (CardView) findViewById(R.id.cardCafe);
        cardViewCafe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                irALogin(R.drawable.cafe_logo,  Constantes.getIP(EmpresasActivity.this));
            }
        });


    }

    private void irALogin(int RLogo, String url) {

        Intent i = new Intent(EmpresasActivity.this, LoginActivity.class);

        i.putExtra("RLogo", RLogo);
        Constantes.setUrl(EmpresasActivity.this, url);

        startActivity(i);

    }

}
