package com.sbc.movil.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbc.adapter.app.ActividadAdapter;
import com.sbc.model.app.Actividad;
import com.sbc.model.app.PersonaArea;
import com.sbc.model.app.dto.Accion;
import com.sbc.movil.R;
import com.sbc.util.Constantes;
import com.sbc.util.JsonTransformer;

import java.util.ArrayList;
import java.util.List;

public class ActividadesActivity extends AppCompatActivity {

    private ActividadAdapter actividadAdapter;

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_actividades_fragment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        AppCompatActivity activity = (AppCompatActivity) this;


        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.parentLayout);
        PersonaArea personaArea = JsonTransformer.fromJSON(Constantes.getUsuario(this), PersonaArea.class);


        TextView textTitulo = (TextView) toolbar.findViewById(R.id.textTitulo);
        textTitulo.setText("Tareas Críticas");

        activity.setSupportActionBar(toolbar);

        Intent intent = getIntent();
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        mLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        int tipo = intent.getIntExtra("tipo",0);

        Accion accion = tipo == 1 ? Accion.INSPECCIONAR :Accion.REFORZAR;

        actividadAdapter = new ActividadAdapter(personaArea.getActividades(), this, accion);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        mRecyclerView.setLayoutAnimation(animation);

        mRecyclerView.setAdapter(actividadAdapter);
    }

    public List<Actividad> cargarActividades() {

        List<Actividad> actividades = new ArrayList<>();


        for (int i = 0; i < 10; i++) {
            int randomNum = 3 + (int) (Math.random() * ((10 - 3) + 1));

            Actividad actividad = new Actividad();
            //   actividad.setNombre("Actividad 0"+(i+1));
            //   actividad.setProcedimientos(randomNum);

            actividades.add(actividad);
        }


        return actividades;
    }

}
