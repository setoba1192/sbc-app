package com.sbc.movil.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;


import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.google.gson.Gson;
import com.sbc.model.app.PersonaArea;
import com.sbc.model.app.Recordatorio;
import com.sbc.model.app.Usuario;
import com.sbc.model.app.dto.Respuesta;
import com.sbc.movil.AppController;
import com.sbc.movil.R;
import com.sbc.util.Constantes;
import com.sbc.util.JsonTransformer;
import com.sbc.util.MyBroadcastReceiver;


import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.Constants;
import cn.pedant.SweetAlert.SweetAlertDialog;
import mehdi.sakout.fancybuttons.FancyButton;

public class LoginActivity extends AppCompatActivity {

    private FancyButton btnIniciarSession;
    private SweetAlertDialog pDialog;
    private PersonaArea personaArea;

    private ImageView imageViewLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Intent intent = getIntent();


        imageViewLogo = (ImageView)findViewById(R.id.imageViewLogo);
        imageViewLogo.setImageDrawable(getResources().getDrawable(R.drawable.logosplash));

        btnIniciarSession = (FancyButton) findViewById(R.id.btnIniciarSesion);
        btnIniciarSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                //  Intent i = new Intent(LoginActivity.this, PrincipalActivity.class);
                //startActivity(i);
                EditText editTextUsuario = (EditText) findViewById(R.id.editTextUsuario);
                EditText editTextPassword = (EditText) findViewById(R.id.editTextPassword);

                if (editTextUsuario.getText().toString().isEmpty()) {

                    editTextUsuario.setError("Debe ingresar el usuario");
                    return;

                }
                if (editTextPassword.getText().toString().isEmpty()) {

                    editTextPassword.setError("Debe ingresar la contraseña");
                    return;
                }
                login(editTextUsuario.getText().toString(), editTextPassword.getText().toString());

            }
        });
    }


    public void login(String usuario, String password) {

        final String us = new String(Base64.encodeToString(usuario.getBytes(), Base64.DEFAULT));
        final String pass = new String(Base64.encodeToString(password.getBytes(), Base64.DEFAULT));


        pDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#373447"));
        pDialog.setTitleText("Ingresando");
        pDialog.setCancelable(false);
        pDialog.show();


        System.out.print("auth!!!!!!!!!!=" + us + ":" + pass);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, Constantes.IP + "/app/login?auth=" + us + ":" + pass, null, createMyReqSuccessListener(),createMyReqErrorListener());
        jsonRequest.setShouldCache(false);

        // Adding String request to request queue
        AppController.getInstance().addToRequestQueue(jsonRequest, "");

    }

    public void setRecordatorios() {


        Intent intent = new Intent(this, MyBroadcastReceiver.class);


        for (Recordatorio recordatorio : personaArea.getRecordatorios()) {


            if (recordatorio.isActivo()) {
                Date dat = new Date();

                Calendar cal_alarm = Calendar.getInstance();
                Calendar cal_now = Calendar.getInstance();
                cal_now.setTime(recordatorio.getRec_hora());


                cal_alarm.setTime(dat);
                cal_alarm.set(Calendar.DAY_OF_WEEK, recordatorio.getDia().getDia_dia_semana());
                cal_alarm.set(Calendar.HOUR_OF_DAY, cal_now.get(Calendar.HOUR_OF_DAY));
                //cal_alarm.set(Calendar.AM_PM, cal_now.get(Calendar.AM_PM));
                cal_alarm.set(Calendar.MINUTE, cal_now.get(Calendar.MINUTE));
                cal_alarm.set(Calendar.SECOND, 0);
                if (cal_alarm.getTimeInMillis() < System.currentTimeMillis()) {
                    cal_alarm.add(Calendar.DAY_OF_YEAR, 7);
                }

                intent.putExtra("dia", recordatorio.getDia().getDia_nombre());

                PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), recordatorio.getRec_codigo(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

                System.out.println("Recordatorio dia " + recordatorio.getDia().getDia_dia_semana() + " Hora " + cal_now.get(Calendar.HOUR_OF_DAY) + " Minuto : " + cal_now.get(Calendar.MINUTE));
                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal_alarm.getTimeInMillis(), 1 * 60 * 60 * 1000, pendingIntent);
            }
        }
    }


    private Response.Listener<JSONObject> createMyReqSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                //TODO: handle success
                System.out.println("Respuesta " + response.toString());

                Respuesta respuesta = JsonTransformer.fromJSON(response.toString(), Respuesta.class);
                if (respuesta.isLogin()) {
                    Constantes.setPreferences(LoginActivity.this, JsonTransformer.toJson(respuesta.getData()));

                    personaArea = JsonTransformer.fromJSON(Constantes.getUsuario(LoginActivity.this), PersonaArea.class);
                    if (personaArea == null) {
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Ingreso")
                                .setContentText("Aún no tiene área asignada.")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismiss();


                                    }
                                })
                                .show();
                        return;
                    }


                    Constantes.setSession(LoginActivity.this, true);
                    setRecordatorios();
                    Intent i = new Intent(LoginActivity.this, PrincipalActivity.class);
                    startActivity(i);
                    finish();
                    return;
                }

                int type = SweetAlertDialog.NORMAL_TYPE;

                if (respuesta.getType() == Respuesta.Type.ERROR) {
                    type = SweetAlertDialog.ERROR_TYPE;
                } else if (respuesta.getType() == Respuesta.Type.WARNING) {

                    type = SweetAlertDialog.WARNING_TYPE;
                } else {
                    type = SweetAlertDialog.SUCCESS_TYPE;
                }

                new SweetAlertDialog(LoginActivity.this, type)
                        .setTitleText("Ingreso")
                        .setContentText(respuesta.getMensaje())
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismiss();


                            }
                        })
                        .show();


                pDialog.dismiss();
            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String mensaje = "Ocurrió un error, intentelo de nuevo...";
                String titulo = "Error";
                int errorType = SweetAlertDialog.ERROR_TYPE;

                if(error.networkResponse.statusCode==400) {
                    String responseBody = null;
                    try {
                        responseBody = new String(error.networkResponse.data, "utf-8");


                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    Respuesta respuesta = JsonTransformer.fromJSON(responseBody, Respuesta.class);

                    mensaje = respuesta.getMensaje();
                    errorType  =SweetAlertDialog.WARNING_TYPE;
                    titulo = "Información";
                }
                error.printStackTrace();
                //TODO: handle failure
                pDialog.dismiss();
                new SweetAlertDialog(LoginActivity.this, errorType)
                        .setTitleText(titulo)
                        .setContentText(mensaje)
                        .show();
            }
        };
    }

}
