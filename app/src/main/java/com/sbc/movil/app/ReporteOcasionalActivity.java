package com.sbc.movil.app;

import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;

import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.sbc.model.app.ReporteOcasional;
import com.sbc.model.app.dto.Respuesta;
import com.sbc.movil.AppController;
import com.sbc.movil.R;
import com.sbc.util.Constantes;
import com.sbc.util.JsonTransformer;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import mehdi.sakout.fancybuttons.FancyButton;

public class ReporteOcasionalActivity extends Activity {


    private EditText editTextArea;
    private EditText editTextObservacion;
    private SweetAlertDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte_ocasional);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textTitulo = (TextView) toolbar.findViewById(R.id.textTitulo);
        textTitulo.setText("Reporte Ocasional");

        editTextArea = (EditText) findViewById(R.id.editTextArea);

        editTextObservacion = (EditText) findViewById(R.id.editTextObservacion);

        FancyButton buttonEnviarReporte = (FancyButton)findViewById(R.id.btn_enviar_reporte);
        buttonEnviarReporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(editTextArea.getText().toString().trim().isEmpty()){

                    editTextArea.setError("Debe ingresar un área");
                    return;
                }

                if(editTextObservacion.getText().toString().trim().isEmpty()){

                    editTextObservacion.setError("Debe ingresar una observación");
                    return;
                }

                ReporteOcasional reporteOcasional = new ReporteOcasional();

                reporteOcasional.setReo_area(editTextArea.getText().toString().trim());
                reporteOcasional.setReo_observacion(editTextObservacion.getText().toString().trim());

                try {
                    enviarReporte(reporteOcasional);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public void enviarReporte(ReporteOcasional reporteOcasional) throws JSONException {


        pDialog = new SweetAlertDialog(ReporteOcasionalActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#373447"));
        pDialog.setTitleText("Enviando...");
        pDialog.setCancelable(false);
        pDialog.show();

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, Constantes.getREPORTEOCASIONAL(this), new JSONObject(JsonTransformer.toJson(reporteOcasional)),createMyReqSuccessListener(),createMyReqErrorListener());

        AppController.getInstance().addToRequestQueue(jsonRequest, "NN");

    }


    private Response.Listener<JSONObject> createMyReqSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                final Respuesta respuesta = JsonTransformer.fromJSON(response.toString(), Respuesta.class);

                int type = SweetAlertDialog.NORMAL_TYPE;

                if (respuesta.getType() == Respuesta.Type.ERROR) {
                    type = SweetAlertDialog.ERROR_TYPE;
                } else if (respuesta.getType() == Respuesta.Type.WARNING) {

                    type = SweetAlertDialog.WARNING_TYPE;
                } else {
                    type = SweetAlertDialog.SUCCESS_TYPE;
                }

                new SweetAlertDialog(ReporteOcasionalActivity.this, type)
                        .setTitleText("Información")
                        .setContentText(respuesta.getMensaje())
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismiss();

                                if (respuesta.getType() == Respuesta.Type.SUCCESS) {
                                    finish();

                                }
                            }
                        })
                        .show();

            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //TODO: handle failure
                pDialog.dismiss();
                new SweetAlertDialog(ReporteOcasionalActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText("Ocurrió un error durante el proceso, intentelo de nuevo...")
                        .show();
            }
        };
    }

}
