package com.sbc.movil.app;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;

import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.sbc.adapter.app.RefuerzoDetalleAdapter;
import com.sbc.model.app.Actividad;
import com.sbc.model.app.Inspeccion;
import com.sbc.model.app.InspeccionDetalle;
import com.sbc.model.app.InspeccionTipo;
import com.sbc.model.app.PersonaArea;
import com.sbc.model.app.Procedimiento;
import com.sbc.model.app.dto.Respuesta;
import com.sbc.movil.AppController;
import com.sbc.movil.R;
import com.sbc.util.Constantes;
import com.sbc.util.JsonTransformer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import mehdi.sakout.fancybuttons.FancyButton;

public class ProcedimientosRefuerzoActivity extends AppCompatActivity {

    private RefuerzoDetalleAdapter refuerzoDetalleAdapter;

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private SweetAlertDialog pDialog;

    private Inspeccion inspeccion;

    private PersonaArea personaArea;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_procedimientos_refuerzo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        AppCompatActivity activity = (AppCompatActivity) this;

        Intent intent = getIntent();


        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.parentLayout);


        TextView textTitulo = (TextView) toolbar.findViewById(R.id.textTitulo);
        textTitulo.setText("Refuerzo");
        textTitulo.setTextColor(Color.parseColor("#2196F3"));

        activity.setSupportActionBar(toolbar);

        FancyButton btnAddInspection = (FancyButton) findViewById(R.id.btn_add_inspection);
        btnAddInspection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finalizarInspeccion();
            }
        });


        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        mLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        int procediments = intent.getIntExtra("procedimientos", 0);
        Actividad actividad = JsonTransformer.fromJSON(intent.getStringExtra("actividad"), Actividad.class);

        inspeccion = new Inspeccion();
        personaArea = JsonTransformer.fromJSON(Constantes.getUsuario(this), PersonaArea.class);
        inspeccion.setArea(personaArea.getArea());
        inspeccion.setPersona(personaArea.getPersona());

        InspeccionTipo tipo = new InspeccionTipo();
        tipo.setInt_codigo(2);

        inspeccion.setInspeccionTipo(tipo);

        inspeccion.setActividad(actividad);
        inspeccion.setInspeccionDetalles(iniciarDetalle(inspeccion));
        refuerzoDetalleAdapter = new RefuerzoDetalleAdapter(inspeccion.getInspeccionDetalles(), this);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        mRecyclerView.setLayoutAnimation(animation);

        mRecyclerView.setAdapter(refuerzoDetalleAdapter);
    }

    public List<InspeccionDetalle> iniciarDetalle(Inspeccion inspeccion) {

        List<InspeccionDetalle> inspeccionDetalles = new ArrayList<>();

        for (Procedimiento procedimiento : inspeccion.getActividad().getProcedimientos()) {

            InspeccionDetalle inspeccionDetalle = new InspeccionDetalle();
            inspeccionDetalle.setProcedimiento(procedimiento);

            inspeccionDetalles.add(inspeccionDetalle);
        }

        return inspeccionDetalles;

    }

    public void finalizarInspeccion() {


        for (InspeccionDetalle inDetalle : inspeccion.getInspeccionDetalles()) {

            if (inDetalle.getCalificacion() == null) {
                new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Calificar")
                        .setContentText("Hay procedimientos pendientes por evaluar.")
                        .show();

                return;
            }
        }

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Inspección")
                .setContentText("¿Está seguro de finalizar la inspección?")
                .setCancelText("Cancelar")
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();

                        try {
                            registrarInspeccion();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .show();

    }

    private void registrarInspeccion() throws JSONException {

        pDialog = new SweetAlertDialog(ProcedimientosRefuerzoActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#373447"));
        pDialog.setTitleText("Registrando...");
        pDialog.setCancelable(false);
        pDialog.show();

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, Constantes.getINSPECCION(this) + "?cal_codigo=1",new JSONObject(JsonTransformer.toJson(inspeccion)),createMyReqSuccessListener(),createMyReqErrorListener());

        AppController.getInstance().addToRequestQueue(jsonRequest, "NN");

    }


    private Response.Listener<JSONObject> createMyReqSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                final Respuesta respuesta = JsonTransformer.fromJSON(response.toString(), Respuesta.class);

                int type = SweetAlertDialog.NORMAL_TYPE;

                if (respuesta.getType() == Respuesta.Type.ERROR) {
                    type = SweetAlertDialog.ERROR_TYPE;
                } else if (respuesta.getType() == Respuesta.Type.WARNING) {

                    type = SweetAlertDialog.WARNING_TYPE;
                } else {
                    type = SweetAlertDialog.SUCCESS_TYPE;
                }

                new SweetAlertDialog(ProcedimientosRefuerzoActivity.this, type)
                        .setTitleText("Información")
                        .setContentText(respuesta.getMensaje())
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismiss();

                                if (respuesta.getType() == Respuesta.Type.SUCCESS) {
                                    finish();

                                }
                            }
                        })
                        .show();
            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //TODO: handle failure
                pDialog.dismiss();
                new SweetAlertDialog(ProcedimientosRefuerzoActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText("Ocurrió un error durante el proceso, intentelo de nuevo...")
                        .show();
            }
        };
    }

}
