package com.sbc.movil.app;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;

import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;


import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.SimpleMultiPartRequest;

import com.sbc.model.app.Inspeccion;
import com.sbc.model.app.InspeccionRefuerzoCheckList;
import com.sbc.model.app.Refuerzo;
import com.sbc.model.app.RefuerzoCheckList;
import com.sbc.model.app.dto.Respuesta;
import com.sbc.movil.AppController;
import com.sbc.movil.R;
import com.sbc.util.Constantes;
import com.sbc.util.JsonTransformer;
import com.sbc.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import cn.pedant.SweetAlert.SweetAlertDialog;
import mehdi.sakout.fancybuttons.FancyButton;


public class RefuerzoActivity extends AppCompatActivity {

    private static final String SBCRecorders = "/SBCRecorders/";

    // ID to identify permission request
    private static final int MY_PERMISSION_REQUEST_ID = 1;

    private SweetAlertDialog pDialog;

    private boolean isUploaded = false;

    String filePath = "";
    int color = 0;
    int requestCode = 0;

    private boolean playing = false;

    private MediaPlayer mp;

    private FancyButton buttonGrabar;
    private FancyButton buttonEscuchar;

    private FancyButton buttonProcedimientos;

    private FancyButton buttonFinalizar;
    private TextView textCantidad;

    private Inspeccion inspeccion;
    private Refuerzo refuerzo;

    private EditText editTextPropuestaMejora;
    private EditText editTextObservacion;

    private TextView textPropuestaMejora;

    int mejora = 0;

    int cumplidos;
    int totales;

    private RefuerzoCheckList refuerzo1;
    private RefuerzoCheckList refuerzo2;
    private RefuerzoCheckList refuerzo3;

    InspeccionRefuerzoCheckList check1;
    InspeccionRefuerzoCheckList check2;
    InspeccionRefuerzoCheckList check3;

    List<InspeccionRefuerzoCheckList> checklist = new ArrayList<>();

    private CheckBox checkBox1;
    private CheckBox checkBox2;
    private CheckBox checkBox3;

    private LinearLayout linearButtonBottom;

    private boolean modoVisualizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refuerzo);

        linearButtonBottom = (LinearLayout) findViewById(R.id.linearButtonBottom);

        check1 = new InspeccionRefuerzoCheckList();
        check2 = new InspeccionRefuerzoCheckList();
        check3 = new InspeccionRefuerzoCheckList();

        refuerzo1 = new RefuerzoCheckList();
        refuerzo1.setId(1);
        refuerzo1.setNombre("Felicitación por cumplimiento");
        refuerzo2 = new RefuerzoCheckList();
        refuerzo2.setId(2);
        refuerzo2.setNombre("Análisis de causas");
        refuerzo3 = new RefuerzoCheckList();
        refuerzo3.setId(3);
        refuerzo3.setNombre("Posibles medidas de mejoramiento");

        check1.setRefuerzoCheckList(refuerzo1);
        check2.setRefuerzoCheckList(refuerzo2);
        check3.setRefuerzoCheckList(refuerzo3);

        checklist.add(check1);
        checklist.add(check2);
        checklist.add(check3);

        checkBox1 = (CheckBox) findViewById(R.id.checkBox1);
        checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                check1.setAplica(b);

            }
        });

        checkBox2 = (CheckBox) findViewById(R.id.checkBox2);
        checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                check2.setAplica(b);

            }
        });

        checkBox3 = (CheckBox) findViewById(R.id.checkBox3);
        checkBox3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                check3.setAplica(b);

            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(
                    new ColorDrawable(ContextCompat.getColor(this, R.color.colorPrimaryDark)));
        }


        Intent intent = getIntent();

        Util.requestPermission(this, Manifest.permission.RECORD_AUDIO);
        Util.requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        cumplidos = intent.getIntExtra("cumplidos", 0);
        totales = intent.getIntExtra("totales", 0);
        inspeccion = JsonTransformer.fromJSON(intent.getStringExtra("inspeccion"), Inspeccion.class);
        refuerzo = new Refuerzo();

        textCantidad = (TextView) findViewById(R.id.textCantidad);
        textPropuestaMejora = (TextView) findViewById(R.id.textPropuestaMejora);
        textPropuestaMejora.setText("Propuesta de mejora (" + cumplidos + " Comportamientos Críticos cumplidos):");

        editTextPropuestaMejora = (EditText) findViewById(R.id.editTextPropuestaMejora);
        editTextObservacion = (EditText) findViewById(R.id.editTextObservacion);

        textCantidad.setText(cumplidos + " de " + totales + " Comportamientos críticos cumplidos.");

        buttonFinalizar = (FancyButton) findViewById(R.id.btn_finalizar);
        buttonFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalizarInspeccion();
            }
        });

        buttonGrabar = (FancyButton) findViewById(R.id.btn_grabar);
        buttonEscuchar = (FancyButton) findViewById(R.id.btn_escuchar);
        buttonEscuchar.setVisibility(View.GONE);

        buttonEscuchar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri uri = Uri.fromFile(new File(filePath));
                mp = MediaPlayer.create(RefuerzoActivity.this, uri);
                if (mp == null) {

                    Toast.makeText(RefuerzoActivity.this, "No hay ningún audio para reproducir", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (playing) {
                    playing = !playing;
                    buttonEscuchar.setText("Escuchar");
                    mp.stop();
                    return;
                }
                playing = !playing;
                buttonEscuchar.setText("Detener");


                mp.start();

                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        playing = !playing;
                        buttonEscuchar.setText("Escuchar");
                    }
                });

            }
        });

        UUID uuid = UUID.randomUUID();
        refuerzo.setAudio(uuid.toString() + ".wav");
        filePath = Environment.getExternalStorageDirectory() + SBCRecorders + uuid.toString() + ".wav";
        // filePath = Environment.getExternalStorageDirectory() + "/SBCRecorders/prueba.mp3";
        color = getResources().getColor(R.color.colorPrimaryDark);
        buttonGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             requestRecordPermission();

            }
        });
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        buttonProcedimientos = (FancyButton) findViewById(R.id.btn_procedimientos);
        buttonProcedimientos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(RefuerzoActivity.this, ProcedimientosActivity.class);
                if (modoVisualizacion) {

                    i.putExtra("inspeccion", JsonTransformer.toJson(inspeccion));
                    i.putExtra("modo", 1);
                    startActivity(i);

                } else {

                    i.putExtra("inspeccion", JsonTransformer.toJson(inspeccion));
                    i.putExtra("modo", 2);
                    startActivityForResult(i,1);


                }
            }
        });

        modoVisualizacion = intent.getBooleanExtra("modoVisualizacion", false);
        if (modoVisualizacion) {

            modoVisualizacion(inspeccion);
        }

    }

    public void subirArchivo() {


        if (!isUploaded) {

            mensaje("Debe grabar un audio", SweetAlertDialog.WARNING_TYPE, false);
            return;
        }
        if (editTextPropuestaMejora.getText().toString().isEmpty()) {
            mensaje("Debe ingresar una propuesta de mejora", SweetAlertDialog.WARNING_TYPE, false);
            return;
        }

        mejora = Integer.parseInt(editTextPropuestaMejora.getText().toString());
        if (mejora > totales) {

            mensaje("Valor propuesta de mejora no permitida", SweetAlertDialog.WARNING_TYPE, false);
            return;
        }

        pDialog = new SweetAlertDialog(RefuerzoActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#373447"));
        pDialog.setTitleText("Subiendo audio...");
        pDialog.setCancelable(false);
        pDialog.show();

        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST, Constantes.getUPLOAD(this),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);


                        pDialog.dismiss();
                        registrarInspeccion();
                        // Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                mensaje("Ocurió un error al subir el archivo.", SweetAlertDialog.ERROR_TYPE, false);
            }
        });

        smr.addFile("file", filePath);
        AppController.getInstance().addToRequestQueue(smr);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {

                isUploaded = true;
                buttonEscuchar.setVisibility(View.VISIBLE);
                // Great! User has recorded and saved the audio file
            } else if (resultCode == RESULT_CANCELED) {
                // Oops! User has canceled the recording
            }
        }

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

                cumplidos = data.getIntExtra("cumplidos", 0);
                totales = data.getIntExtra("totales", 0);
                inspeccion = JsonTransformer.fromJSON(data.getStringExtra("inspeccion"), Inspeccion.class);
                textPropuestaMejora.setText("Propuesta de mejora (" + cumplidos + " Comportamientos Críticos cumplidos):");
                textCantidad.setText(cumplidos + " de " + totales + " Comportamientos críticos cumplidos.");
            }
        }
    }

    public void mensaje(String mensaje, int type, final boolean finish) {

        new SweetAlertDialog(RefuerzoActivity.this, type)
                .setTitleText("Información")
                .setContentText(mensaje)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();

                        if (finish) {

                            finish();
                        }

                    }
                })
                .show();
    }

    public void finalizarInspeccion() {

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Observación")
                .setContentText("¿Está seguro de finalizar la observación?")
                .setCancelText("Cancelar")
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();

                        subirArchivo();
                    }
                })
                .show();
    }

    public void registrarInspeccion() {

        refuerzo.setCheckList(checklist);

        refuerzo.setObservacion(editTextObservacion.getText().toString());
        refuerzo.setPuntajeMaximo(totales);
        refuerzo.setPuntajeObtenido(cumplidos);
        refuerzo.setPuntajeMejora(mejora);


        inspeccion.setRefuerzo(refuerzo);
        pDialog = new SweetAlertDialog(RefuerzoActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#373447"));
        pDialog.setTitleText("Registrando...");
        pDialog.setCancelable(false);
        pDialog.show();
        try {
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, Constantes.getINSPECCION(this) + "?cal_codigo=1", new JSONObject(JsonTransformer.toJson(inspeccion)), createMyReqSuccessListener(), createMyReqErrorListener());
            AppController.getInstance().addToRequestQueue(jsonRequest, "NN");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Response.Listener<JSONObject> createMyReqSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                pDialog.dismiss();
                final Respuesta respuesta = JsonTransformer.fromJSON(response.toString(), Respuesta.class);

                int type = SweetAlertDialog.NORMAL_TYPE;

                if (respuesta.getType() == Respuesta.Type.ERROR) {
                    type = SweetAlertDialog.ERROR_TYPE;
                } else if (respuesta.getType() == Respuesta.Type.WARNING) {

                    type = SweetAlertDialog.WARNING_TYPE;
                } else {
                    type = SweetAlertDialog.SUCCESS_TYPE;
                    NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancelAll();

                }

                new SweetAlertDialog(RefuerzoActivity.this, type)
                        .setTitleText("Información")
                        .setContentText(respuesta.getMensaje())
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismiss();


                                if (respuesta.getType() == Respuesta.Type.SUCCESS) {
                                    finish();

                                }
                            }
                        })
                        .show();

            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                mensaje("Ocurrió un error durante el registro, intentelo de nuevo.", SweetAlertDialog.ERROR_TYPE, false);
            }
        };
    }

    @Override
    public void onBackPressed() {

        if (!modoVisualizacion) {


            new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Información")
                    .setContentText("¿Desea abandonar la observación?")
                    .setCancelText("Cancelar")
                    .setConfirmText("Aceptar")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismiss();

                            finish();
                        }
                    })
                    .show();
        } else {

            super.onBackPressed();
        }
    }


    public void modoVisualizacion(Inspeccion inspeccion) {


        buttonGrabar.setVisibility(View.GONE);
        //linearButtonBottom.setVisibility(View.GONE);
        editTextPropuestaMejora.setEnabled(false);
        editTextObservacion.setEnabled(false);

        checkBox1.setClickable(false);
        checkBox2.setClickable(false);
        checkBox3.setClickable(false);


        buttonEscuchar.setVisibility(View.VISIBLE);
        buttonFinalizar.setText("Volver");

        buttonFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });


        if (inspeccion.getRefuerzo() != null) {
            editTextObservacion.setHint(inspeccion.getRefuerzo().getObservacion().isEmpty() ? "Ningúna observación" : inspeccion.getRefuerzo().getObservacion());

            filePath = filePath = Environment.getExternalStorageDirectory() + SBCRecorders + inspeccion.getRefuerzo().getAudio();
            editTextPropuestaMejora.setText(Integer.toString(inspeccion.getRefuerzo().getPuntajeMejora()));


            checkBox1.setChecked(inspeccion.getRefuerzo().getCheckList().get(0).isAplica());
            checkBox2.setChecked(inspeccion.getRefuerzo().getCheckList().get(1).isAplica());
            checkBox3.setChecked(inspeccion.getRefuerzo().getCheckList().get(2).isAplica());
        }

    }


    private void requestRecordPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSION_REQUEST_ID);
        } else {
            empezarGrabacion();
        }
    }

    private void empezarGrabacion(){
        AndroidAudioRecorder.with(RefuerzoActivity.this)
                // Required

                .setFilePath(filePath)
                .setColor(color)
                .setRequestCode(requestCode)

                // Optional
                .setSource(AudioSource.MIC)
                .setChannel(AudioChannel.STEREO)
                .setSampleRate(AudioSampleRate.HZ_48000)
                .setAutoStart(false)
                .setKeepDisplayOn(true)

                // Start recording
                .record();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_ID: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // granted, start OmRecorder
                    this.empezarGrabacion();
                }
                return;
            }
        }
    }

}
