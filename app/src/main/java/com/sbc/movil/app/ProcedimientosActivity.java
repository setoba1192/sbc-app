package com.sbc.movil.app;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;


import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;

import com.sbc.adapter.app.InspeccionDetalleAdapter;
import com.sbc.model.app.Actividad;
import com.sbc.model.app.Calificacion;
import com.sbc.model.app.Inspeccion;
import com.sbc.model.app.InspeccionDetalle;
import com.sbc.model.app.InspeccionTipo;
import com.sbc.model.app.PersonaArea;
import com.sbc.model.app.Procedimiento;
import com.sbc.model.app.dto.Respuesta;
import com.sbc.movil.AppController;
import com.sbc.movil.R;
import com.sbc.util.Constantes;
import com.sbc.util.JsonTransformer;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import mehdi.sakout.fancybuttons.FancyButton;

public class ProcedimientosActivity extends AppCompatActivity {

    private InspeccionDetalleAdapter inspeccionDetalleAdapter;

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private SweetAlertDialog pDialog;


    private Inspeccion inspeccion;

    private PersonaArea personaArea;

    private int modo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_procedimientos);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        AppCompatActivity activity = (AppCompatActivity) this;

        Intent intent = getIntent();

        modo = intent.getIntExtra("modo", 3);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.parentLayout);

        FancyButton btnAddInspection = (FancyButton) findViewById(R.id.btn_add_inspection);
        btnAddInspection.setText(modo==1 ? "Salir" : modo ==2 ? "Terminar" : "Siguiente");
        btnAddInspection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finalizarInspeccion();
            }
        });


        inspeccion = new Inspeccion();
        personaArea = JsonTransformer.fromJSON(Constantes.getUsuario(this), PersonaArea.class);
        inspeccion.setArea(personaArea.getArea());
        inspeccion.setPersona(personaArea.getPersona());

        InspeccionTipo tipo = new InspeccionTipo();
        tipo.setInt_codigo(1);

        inspeccion.setInspeccionTipo(tipo);


        TextView textTitulo = (TextView) toolbar.findViewById(R.id.textTitulo);
        textTitulo.setText("Observación");

        activity.setSupportActionBar(toolbar);


        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        mLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        int procediments = intent.getIntExtra("procedimientos", 0);

        /**
         * Modo nuevos procedimientos
         */
        if (modo == 3) {
            Actividad actividad = JsonTransformer.fromJSON(intent.getStringExtra("actividad"), Actividad.class);
            inspeccion.setActividad(actividad);
        }

        /**
         * Iniciar detalle
         */


        //Modo 1 visualización
        if (modo == 1) {

            inspeccion = JsonTransformer.fromJSON(intent.getStringExtra("inspeccion"), Inspeccion.class);
            //inspeccion.setInspeccionDetalles(iniciarDetalle(inspeccion));
            //Modo 2 edición
        } else if (modo == 2) {
            inspeccion = JsonTransformer.fromJSON(intent.getStringExtra("inspeccion"), Inspeccion.class);
            //inspeccion.setInspeccionDetalles(iniciarDetalle(inspeccion));
            //Modo 3 nuevo
        } else if (modo == 3) {
            inspeccion.setInspeccionDetalles(iniciarDetalle(inspeccion));
        }


        //System.out.println("Procedimientos " + procediments);
        inspeccionDetalleAdapter = new InspeccionDetalleAdapter(inspeccion.getInspeccionDetalles(), this, modo);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        mRecyclerView.setLayoutAnimation(animation);

        mRecyclerView.setAdapter(inspeccionDetalleAdapter);


        //System.out.print("Hola mundo");
        //System.out.print("Hola mundo2 " + JsonTransformer.toJson(inspeccion));
    }

    public List<InspeccionDetalle> iniciarDetalle(Inspeccion inspeccion) {

        List<InspeccionDetalle> inspeccionDetalles = new ArrayList<>();

        for (Procedimiento procedimiento : inspeccion.getActividad().getProcedimientos()) {

            InspeccionDetalle inspeccionDetalle = new InspeccionDetalle();
            inspeccionDetalle.setProcedimiento(procedimiento);

            inspeccionDetalles.add(inspeccionDetalle);
        }

        return inspeccionDetalles;

    }

    public void finalizarInspeccion() {


        for (InspeccionDetalle inDetalle : inspeccion.getInspeccionDetalles()) {

            if (inDetalle.getCalificacion() == null) {
                new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Calificar")
                        .setContentText("Hay Comportamientos críticos pendientes por evaluar.")
                        .show();

                return;
            }
        }

        int valores[] = calculoProcedimientos();


        System.out.println("Cumple " + valores[0] + " De " + valores[1]);


        //Modo normal, continua para refuerzo
        if (modo == 3) {
            Intent i = new Intent(ProcedimientosActivity.this, RefuerzoActivity.class);
            i.putExtra("cumplidos", valores[0]);
            i.putExtra("totales", valores[1]);
            i.putExtra("inspeccion", JsonTransformer.toJson(inspeccion));
            startActivity(i);

            finish();
        }
        //Modo visuzalización, finalizar
        if(modo == 1){

            finish();

        }
        if(modo == 2){

            Intent returnIntent = new Intent();

            returnIntent.putExtra("cumplidos", valores[0]);
            returnIntent.putExtra("totales", valores[1]);
            returnIntent.putExtra("inspeccion", JsonTransformer.toJson(inspeccion));
            setResult(Activity.RESULT_OK,returnIntent);
            finish();

        }


       /* new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Inspección")
                .setContentText("¿Está seguro de finalizar la inspección?")
                .setCancelText("Cancelar")
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();

                        registrarInspeccion();
                    }
                })
                .show();
*/
    }

    private void registrarInspeccion() {

        pDialog = new SweetAlertDialog(ProcedimientosActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#373447"));
        pDialog.setTitleText("Registrando...");
        pDialog.setCancelable(false);
        pDialog.show();
/*
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest()

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, Constantes.INSPECCION + "?cal_codigo=1", JsonTransformer.toJson(inspeccion), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //TODO: handle success
                pDialog.dismiss();
                final Respuesta respuesta = JsonTransformer.fromJSON(response.toString(), Respuesta.class);

                int type = SweetAlertDialog.NORMAL_TYPE;

                if (respuesta.getType() == Respuesta.Type.ERROR) {
                    type = SweetAlertDialog.ERROR_TYPE;
                } else if (respuesta.getType() == Respuesta.Type.WARNING) {

                    type = SweetAlertDialog.WARNING_TYPE;
                } else {
                    type = SweetAlertDialog.SUCCESS_TYPE;
                }

                new SweetAlertDialog(ProcedimientosActivity.this, type)
                        .setTitleText("Información")
                        .setContentText(respuesta.getMensaje())
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismiss();


                                if (respuesta.getType() == Respuesta.Type.SUCCESS) {
                                    finish();

                                }
                            }
                        })
                        .show();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //TODO: handle failure
                pDialog.dismiss();
                new SweetAlertDialog(ProcedimientosActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText("Ocurrió un error durante el proceso, intentelo de nuevo...")
                        .show();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonRequest, "NN");
*/
    }

    /**
     * Método para calcular cuantos de cuantos procedimientos aplicaron.
     *
     * @return
     */
    public int[] calculoProcedimientos() {

        int total = 0;

        int cumple = 0;

        for (InspeccionDetalle det : inspeccion.getInspeccionDetalles()) {

            if (det.getCalificacion().getCal_codigo() == 1) {

                total++;
                cumple++;
            }
            if (det.getCalificacion().getCal_codigo() == 2) {
                total++;
            }
        }

        int datos[] = {cumple, total};

        return datos;
    }

}
