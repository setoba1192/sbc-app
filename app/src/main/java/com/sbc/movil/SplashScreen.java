package com.sbc.movil;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.sbc.movil.app.PrincipalActivity;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashScreen extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        final ImageView imageSplash = (ImageView)findViewById(R.id.imageSplash);
        imageSplash.setVisibility(View.GONE);

        YoYo.with(Techniques.Swing)
                .duration(1300).onStart(new YoYo.AnimatorCallback() {
            @Override
            public void call(Animator animator) {
               imageSplash.setVisibility(View.VISIBLE);
            }
        }).onEnd(new YoYo.AnimatorCallback() {
            @Override
            public void call(Animator animator) {
                Intent i = new Intent(SplashScreen.this, PrincipalActivity.class);
                startActivity(i);

                finish();
            }
        })
                .playOn(imageSplash);


    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }


}
