package com.sbc.adapter.app;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbc.model.app.Calificacion;
import com.sbc.model.app.InspeccionDetalle;
import com.sbc.movil.R;

import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Luna on 27/01/2018.
 */

public class RefuerzoDetalleAdapter extends RecyclerView.Adapter<RefuerzoDetalleAdapter.InspeccionDetalleViewHolder> {

    private List<InspeccionDetalle> procedimientos;
    private Context context;

    public RefuerzoDetalleAdapter(List<InspeccionDetalle> procedimientos, Context context) {
        this.procedimientos = procedimientos;
        this.context = context;
    }


    @Override
    public RefuerzoDetalleAdapter.InspeccionDetalleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.app_procedimiento_refuerzo_item, parent, false);

        return new RefuerzoDetalleAdapter.InspeccionDetalleViewHolder(inflatedView, context);
    }

    @Override
    public void onBindViewHolder(RefuerzoDetalleAdapter.InspeccionDetalleViewHolder holder, int position) {
        InspeccionDetalle procedimiento = procedimientos.get(position);
        holder.bindInspeccionDetalle(procedimiento);

    }

    @Override
    public int getItemCount() {
        return procedimientos.size();
    }


    public static class InspeccionDetalleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //2

        private TextView textInspeccionDetalle;
        private InspeccionDetalle inspeccionDetalle;
        private Context context;

        private FancyButton btnMejoro;
        private FancyButton btnNoMejoro;
        private FancyButton btnNoAplica;

        private TextView editTextArguments;

        private RelativeLayout procedimientoBackground;

        //4
        public InspeccionDetalleViewHolder(View v, final Context context) {
            super(v);

            this.context = context;


            final int colorCumple = context.getResources().getColor(R.color.colorCumple);
            final int colorNoCumple = context.getResources().getColor(R.color.colorNoCumple);
            final int colorNoAplica = context.getResources().getColor(R.color.colorNoAplica);

            final int colorBlanco = context.getResources().getColor(R.color.white);
            final int colorNegro = context.getResources().getColor(R.color.white);

            editTextArguments = (TextView) v.findViewById(R.id.editTextArguments);
            editTextArguments.setVisibility(View.GONE);

            editTextArguments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setArguments(editTextArguments, context, inspeccionDetalle);
                }
            });

            textInspeccionDetalle = (TextView) v.findViewById(R.id.textProcedimiento);


            procedimientoBackground = (RelativeLayout) v.findViewById(R.id.procedimientoBackground);

            btnMejoro = (FancyButton) v.findViewById(R.id.btnMejoro);
            btnMejoro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editTextArguments.setVisibility(View.GONE);
                    cambiarColorBackground(procedimientoBackground, textInspeccionDetalle, colorNegro, colorCumple, colorBlanco);

                    Calificacion calificacion = new Calificacion();
                    calificacion.setCal_codigo(3);
                    inspeccionDetalle.setCalificacion(calificacion);
                }
            });

            btnNoMejoro = (FancyButton) v.findViewById(R.id.btnNoMejoro);
            btnNoMejoro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editTextArguments.setVisibility(View.VISIBLE);
                    cambiarColorBackground(procedimientoBackground, textInspeccionDetalle, colorNegro, colorNoCumple, colorBlanco);

                    Calificacion calificacion = new Calificacion();
                    calificacion.setCal_codigo(4);
                    inspeccionDetalle.setCalificacion(calificacion);
                }
            });
            btnNoAplica = (FancyButton) v.findViewById(R.id.btnNoAplica);
            btnNoAplica.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editTextArguments.setVisibility(View.VISIBLE);
                    cambiarColorBackground(procedimientoBackground, textInspeccionDetalle, colorNegro, colorNoAplica, colorBlanco);

                    Calificacion calificacion = new Calificacion();
                    calificacion.setCal_codigo(3);
                    inspeccionDetalle.setCalificacion(calificacion);
                }
            });


            //v.setOnClickListener(this);
        }

        //5
        @Override
        public void onClick(View v) {
            Log.d("RecyclerView", "CLICK!");

            //  NormalListDialogCustomAttr(this.inspeccion.getInspeccionDetalles());
        }

        public void bindInspeccionDetalle(InspeccionDetalle inspeccionDetalle) {

            this.inspeccionDetalle = inspeccionDetalle;

            textInspeccionDetalle.setText(inspeccionDetalle.getProcedimiento().getPro_nombre());

        }

        public void setArguments(final TextView textView, Context context, final InspeccionDetalle inspeccionDetalle) {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Ingresar un comentario");

// Set up the input
            final EditText input = new EditText(context);
            int maxLength = 250;
            InputFilter[] fArray = new InputFilter[1];
            fArray[0] = new InputFilter.LengthFilter(maxLength);
            input.setFilters(fArray);
            input.setHint(textView.getText().toString());
            input.setText(inspeccionDetalle.getInd_observacion());
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            input.setSingleLine(false);
            input.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION | EditorInfo.TYPE_TEXT_FLAG_AUTO_CORRECT);

            builder.setView(input);

// Set up the buttons
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    inspeccionDetalle.setInd_observacion(input.getText().toString().trim());
                    textView.setText(inspeccionDetalle.getInd_observacion());
                }
            });
            builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        }

        public void cambiarColorBackground(final View v, final View texto, int colorNegro, int toColor, int colorBlanco) {

            ColorDrawable actualColor = (ColorDrawable) v.getBackground();
            int colorFrom = actualColor.getColor();

            final TextView procedimientoTitulo = (TextView) texto;

            ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, toColor);

            colorAnimation.setDuration(500); // milliseconds
            colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    v.setBackgroundColor((int) animator.getAnimatedValue());
                }

            });


            ValueAnimator colorAnimationTexto = ValueAnimator.ofObject(new ArgbEvaluator(), colorNegro, colorBlanco);

            colorAnimationTexto.setDuration(500); // milliseconds
            colorAnimationTexto.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    procedimientoTitulo.setTextColor((int) animator.getAnimatedValue());
                }

            });

            colorAnimation.start();
            colorAnimationTexto.start();

        }
    }
}
