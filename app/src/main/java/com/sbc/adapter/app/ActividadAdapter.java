package com.sbc.adapter.app;

import android.app.Activity;
import android.content.Context;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.flippers.FlipInYAnimator;
import com.flyco.animation.Attention.Tada;
import com.flyco.animation.BaseAnimatorSet;
import com.flyco.animation.FadeEnter.FadeEnter;
import com.flyco.dialog.entity.DialogMenuItem;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.NormalListDialog;
import com.sbc.model.app.Actividad;

import com.sbc.model.app.Procedimiento;
import com.sbc.model.app.dto.Accion;
import com.sbc.movil.R;
import com.sbc.movil.app.ProcedimientosActivity;
import com.sbc.movil.app.ProcedimientosRefuerzoActivity;
import com.sbc.util.JsonTransformer;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by JACS1 on 22/11/2016.
 */

public class ActividadAdapter extends RecyclerView.Adapter<ActividadAdapter.ActividadViewHolder> {

    private List<Actividad> actividades;
    private Context context;
    private Accion accion;

    public ActividadAdapter(List<Actividad> actividades, Context context, Accion accion) {
        this.actividades = actividades;
        this.context = context;
        this.accion = accion;
    }


    @Override
    public ActividadAdapter.ActividadViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.app_actividad_item, parent, false);

        return new ActividadViewHolder(inflatedView, context, accion);
    }

    @Override
    public void onBindViewHolder(ActividadAdapter.ActividadViewHolder holder, int position) {

        Actividad actividad = actividades.get(position);
        holder.bindActividad(actividad);

    }

    @Override
    public int getItemCount() {
        return actividades.size();
    }


    public static class ActividadViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //2
        private ImageView imageActividad;
        private TextView textProcesos;
        private TextView textNombre;
        private Actividad actividad;
        private Context context;

        //4
        public ActividadViewHolder(View v, final Context context, Accion accion) {
            super(v);

            this.context = context;
            imageActividad = (ImageView) v.findViewById(R.id.imageActividad);

            textProcesos = (TextView) v.findViewById(R.id.textProcesos);
            textNombre = (TextView) v.findViewById(R.id.textActividad);

            if (accion == Accion.VISIBLE) {
                v.setOnClickListener(this);
            }
            if (accion == Accion.INSPECCIONAR) {
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        Intent i = new Intent(context, ProcedimientosActivity.class);

                        i.putExtra("procedimientos", actividad.getProcedimientos().size());
                        i.putExtra("actividad", JsonTransformer.toJson(actividad));
                        context.startActivity(i);
                        ((Activity) context).finish();

                    }
                });
            }

            if (accion == Accion.REFORZAR) {
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        Intent i = new Intent(context, ProcedimientosRefuerzoActivity.class);

                        i.putExtra("procedimientos", actividad.getProcedimientos().size());
                        i.putExtra("actividad", JsonTransformer.toJson(actividad));
                        context.startActivity(i);
                        ((Activity) context).finish();

                    }
                });
            }
        }

        //5
        @Override
        public void onClick(View v) {
            Log.d("RecyclerView", "CLICK!");

            NormalListDialogCustomAttr(this.actividad.getProcedimientos());
        }

        public void bindActividad(Actividad actividad) {
            this.actividad = actividad;


            this.textNombre.setText(actividad.getAct_nombre());
            this.textProcesos.setText("Comportamientos Críticos: " + actividad.getProcedimientos().size());
            /*
            Ion.with(this.context)
                    .load(Constantes.URL_PHOTO+"?per_codigo="+actividad.getDocente_codigo()).noCache()
                    .setHeader("Cookie", this.loginUser.getSession())
                    .withBitmap().asBitmap().setCallback(new FutureCallback<Bitmap>() {
                @Override
                public void onCompleted(Exception e, Bitmap result) {
                    imageDocente.setImageBitmap(result);

                }
            });
*/
        }

        private void NormalListDialogCustomAttr(List<Procedimiento> procedimientos) {

            ArrayList<DialogMenuItem> lista = new ArrayList<>();
            for (Procedimiento pro : procedimientos) {

                lista.add(new DialogMenuItem((pro.getPro_nombre()), R.drawable.ic_process));

            }

            final NormalListDialog dialog = new NormalListDialog(context, lista);
            dialog.title("Procedimientos")//
                    .titleTextSize_SP(18)//
                    .titleTextColor(Color.WHITE)
                    .titleBgColor(Color.BLACK)//
                    .itemPressColor(Color.parseColor("#85D3EF"))//
                    .itemTextColor(Color.parseColor("#303030"))//
                    .itemTextSize(14)//

                    .cornerRadius(0)//
                    .widthScale(0.8f)//

                    .show();

            dialog.setOnOperItemClickL(new OnOperItemClickL() {
                @Override
                public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //T.showShort(context, mMenuItems.get(position).mOperName);
                    dialog.dismiss();
                }
            });
        }
    }
}
