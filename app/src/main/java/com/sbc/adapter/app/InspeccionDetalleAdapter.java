package com.sbc.adapter.app;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbc.model.app.Actividad;
import com.sbc.model.app.Calificacion;
import com.sbc.model.app.Inspeccion;
import com.sbc.model.app.InspeccionDetalle;
import com.sbc.movil.R;

import org.w3c.dom.Text;

import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Luna on 27/01/2018.
 */

public class InspeccionDetalleAdapter extends RecyclerView.Adapter<InspeccionDetalleAdapter.InspeccionDetalleViewHolder> {

    private List<InspeccionDetalle> procedimientos;
    private Context context;
    private int modo;

    public InspeccionDetalleAdapter(List<InspeccionDetalle> procedimientos, Context context, int modo) {
        this.procedimientos = procedimientos;
        this.context = context;
        this.modo = modo;
    }


    @Override
    public InspeccionDetalleAdapter.InspeccionDetalleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.app_procedimiento_item, parent, false);

        return new InspeccionDetalleAdapter.InspeccionDetalleViewHolder(inflatedView, context);
    }

    @Override
    public void onBindViewHolder(InspeccionDetalleAdapter.InspeccionDetalleViewHolder holder, int position) {
        InspeccionDetalle procedimiento = procedimientos.get(position);
        holder.bindInspeccionDetalle(procedimiento, modo);

    }

    @Override
    public int getItemCount() {
        return procedimientos.size();
    }


    public static class InspeccionDetalleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //2

        private TextView textInspeccionDetalle;
        private InspeccionDetalle inspeccionDetalle;
        private Context context;

        private FancyButton btnCumple;
        private FancyButton btnNoCumple;
        private FancyButton btnNoAplica;

        private TextView editTextArguments;

        private RelativeLayout procedimientoBackground;

        //4
        public InspeccionDetalleViewHolder(View v, final Context context) {
            super(v);

            this.context = context;


            final int colorCumple = context.getResources().getColor(R.color.colorCumple);
            final int colorNoCumple = context.getResources().getColor(R.color.colorNoCumple);
            final int colorNoAplica = context.getResources().getColor(R.color.colorNoAplica);

            final int colorBlanco = context.getResources().getColor(R.color.white);
            final int colorNegro = context.getResources().getColor(R.color.white);

            editTextArguments = (TextView) v.findViewById(R.id.editTextArguments);
            editTextArguments.setVisibility(View.GONE);

            editTextArguments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setArguments(editTextArguments, context, inspeccionDetalle);
                }
            });

            textInspeccionDetalle = (TextView) v.findViewById(R.id.textProcedimiento);


            procedimientoBackground = (RelativeLayout) v.findViewById(R.id.procedimientoBackground);

            btnCumple = (FancyButton) v.findViewById(R.id.btnCumple);
            btnCumple.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editTextArguments.setVisibility(View.GONE);
                    cambiarColorBackground(procedimientoBackground, textInspeccionDetalle, colorNegro, colorCumple, colorBlanco);

                    Calificacion calificacion = new Calificacion();
                    calificacion.setCal_codigo(1);
                    inspeccionDetalle.setCalificacion(calificacion);
                }
            });

            btnNoCumple = (FancyButton) v.findViewById(R.id.btnNoCumple);
            btnNoCumple.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editTextArguments.setVisibility(View.VISIBLE);
                    cambiarColorBackground(procedimientoBackground, textInspeccionDetalle, colorNegro, colorNoCumple, colorBlanco);

                    Calificacion calificacion = new Calificacion();
                    calificacion.setCal_codigo(2);
                    inspeccionDetalle.setCalificacion(calificacion);
                }
            });
            btnNoAplica = (FancyButton) v.findViewById(R.id.btnNoAplica);
            btnNoAplica.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editTextArguments.setVisibility(View.VISIBLE);
                    cambiarColorBackground(procedimientoBackground, textInspeccionDetalle, colorNegro, colorNoAplica, colorBlanco);

                    Calificacion calificacion = new Calificacion();
                    calificacion.setCal_codigo(3);
                    inspeccionDetalle.setCalificacion(calificacion);
                }
            });


            //v.setOnClickListener(this);
        }

        //5
        @Override
        public void onClick(View v) {
            Log.d("RecyclerView", "CLICK!");

            //  NormalListDialogCustomAttr(this.inspeccion.getInspeccionDetalles());
        }

        public void bindInspeccionDetalle(InspeccionDetalle inspeccionDetalle, int modo) {

            final int colorCumple = context.getResources().getColor(R.color.colorCumple);
            final int colorNoCumple = context.getResources().getColor(R.color.colorNoCumple);
            final int colorNoAplica = context.getResources().getColor(R.color.colorNoAplica);

            final int colorBlanco = context.getResources().getColor(R.color.white);
            final int colorNegro = context.getResources().getColor(R.color.white);


            this.inspeccionDetalle = inspeccionDetalle;

            textInspeccionDetalle.setText(inspeccionDetalle.getProcedimiento().getPro_nombre());

            //No hacer nada-- Mantener la función original
            if (modo == 3) {

                return;
            }


            int color = inspeccionDetalle.getCalificacion().getCal_codigo() == 1 ? colorCumple : inspeccionDetalle.getCalificacion().getCal_codigo() == 2 ? colorNoCumple : colorNoAplica;
            cambiarColorBackground(procedimientoBackground, textInspeccionDetalle, colorNegro, color, colorBlanco);
            editTextArguments.setText(inspeccionDetalle.getInd_observacion()==null ? "" : inspeccionDetalle.getInd_observacion());

            editTextArguments.setVisibility(inspeccionDetalle.getCalificacion().getCal_codigo()==2 ? View.VISIBLE  : inspeccionDetalle.getCalificacion().getCal_codigo()==3 ? View.VISIBLE : View.INVISIBLE);
            if (modo == 1) {

                btnCumple.setClickable(false);
                btnNoCumple.setClickable(false);
                btnNoAplica.setClickable(false);
                editTextArguments.setClickable(false);
            }
            //Programar el modo 2, con activityforresult para contabilizar nuevamente los procedimientos cumplidos.




        }

        public void setArguments(final TextView textView, Context context, final InspeccionDetalle inspeccionDetalle) {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Ingresar un comentario");

// Set up the input
            final EditText input = new EditText(context);
            int maxLength = 250;
            InputFilter[] fArray = new InputFilter[1];
            fArray[0] = new InputFilter.LengthFilter(maxLength);
            input.setFilters(fArray);
            input.setHint(textView.getText().toString());
            input.setText(inspeccionDetalle.getInd_observacion());
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            input.setSingleLine(false);
            input.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION | EditorInfo.TYPE_TEXT_FLAG_AUTO_CORRECT);

            builder.setView(input);

// Set up the buttons
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    inspeccionDetalle.setInd_observacion(input.getText().toString().trim());
                    textView.setText(inspeccionDetalle.getInd_observacion());
                }
            });
            builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        }

        public void cambiarColorBackground(final View v, final View texto, int colorNegro, int toColor, int colorBlanco) {

            ColorDrawable actualColor = (ColorDrawable) v.getBackground();
            int colorFrom = actualColor.getColor();

            final TextView procedimientoTitulo = (TextView) texto;

            ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, toColor);

            colorAnimation.setDuration(500); // milliseconds
            colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    v.setBackgroundColor((int) animator.getAnimatedValue());
                }

            });


            ValueAnimator colorAnimationTexto = ValueAnimator.ofObject(new ArgbEvaluator(), colorNegro, colorBlanco);

            colorAnimationTexto.setDuration(500); // milliseconds
            colorAnimationTexto.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    procedimientoTitulo.setTextColor((int) animator.getAnimatedValue());
                }

            });

            colorAnimation.start();
            colorAnimationTexto.start();

        }
    }
}
