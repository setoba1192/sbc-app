package com.sbc.adapter.app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyco.dialog.entity.DialogMenuItem;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.NormalListDialog;
import com.sbc.model.app.Inspeccion;
import com.sbc.model.app.Inspeccion;
import com.sbc.model.app.InspeccionDetalle;
import com.sbc.movil.R;
import com.sbc.movil.app.ProcedimientosActivity;
import com.sbc.movil.app.RefuerzoActivity;
import com.sbc.util.JsonTransformer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JACS1 on 22/11/2016.
 */

public class InspeccionAdapter extends RecyclerView.Adapter<InspeccionAdapter.InspeccionViewHolder> {

    private List<Inspeccion> inspecciones;
    private Context context;

    public InspeccionAdapter(List<Inspeccion> inspecciones, Context context) {
        this.inspecciones = inspecciones;
        this.context = context;
    }




    @Override
    public InspeccionAdapter.InspeccionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.app_inspeccion_item, parent, false);

        return new InspeccionViewHolder(inflatedView, context);
    }

    @Override
    public void onBindViewHolder(InspeccionAdapter.InspeccionViewHolder holder, int position) {

        Inspeccion inspeccion = inspecciones.get(position);
        holder.bindInspeccion(inspeccion);

    }

    @Override
    public int getItemCount() {
        return inspecciones.size();
    }



    public static class InspeccionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //2
        private ImageView imageInspeccion;
        private TextView textFecha;
        private TextView textActividad;
        private Inspeccion inspeccion;
        private Context context;

        //4
        public InspeccionViewHolder(View v, Context context) {
            super(v);

            this.context = context;
          //  imageInspeccion= (ImageView) v.findViewById(R.id.imageInspeccion);

            textFecha = (TextView) v.findViewById(R.id.textFecha);
            textActividad = (TextView)v.findViewById(R.id.textActividad) ;
            v.setOnClickListener(this);
        }

        //5
        @Override
        public void onClick(View v) {
            Log.d("RecyclerView", "CLICK!");

            System.out.println("JSON!!! "+ JsonTransformer.toJson(inspeccion));

            int valores[] = calculoProcedimientos();

            System.out.println("Cumple "+valores[0]+" De "+valores[1]);

            Intent i  = new Intent(context, RefuerzoActivity.class);
            i.putExtra("cumplidos",valores[0]);
            i.putExtra("totales",valores[1]);
            i.putExtra("inspeccion", JsonTransformer.toJson(inspeccion));
            i.putExtra("modoVisualizacion", true);
            context.startActivity(i);

        }

        public int[] calculoProcedimientos() {

            int total = 0;

            int cumple = 0;

            for (InspeccionDetalle det : inspeccion.getInspeccionDetalles()) {

                if (det.getCalificacion().getCal_codigo() == 1) {

                    total++;
                    cumple++;
                }
                if (det.getCalificacion().getCal_codigo() == 2) {
                    total++;
                }
            }

            int datos[] = {cumple,total};

            return datos;
        }


        public void bindInspeccion(Inspeccion inspeccion) {
            this.inspeccion = inspeccion;
            DateFormat df = new DateFormat();


            this.textFecha.setText("Fecha: "+df.format("dd-MM-yyyy hh:mm a",inspeccion.getIns_fecha()));
            this.textActividad.setText(inspeccion.getActividad().getAct_nombre());
            /*
            Ion.with(this.context)
                    .load(Constantes.URL_PHOTO+"?per_codigo="+Inspeccion.getDocente_codigo()).noCache()
                    .setHeader("Cookie", this.loginUser.getSession())
                    .withBitmap().asBitmap().setCallback(new FutureCallback<Bitmap>() {
                @Override
                public void onCompleted(Exception e, Bitmap result) {
                    imageDocente.setImageBitmap(result);

                }
            });
*/
        }

        private void NormalListDialogCustomAttr(int procedimientos) {

            ArrayList<DialogMenuItem> lista = new ArrayList<>();
            for(int i =0; i<procedimientos; i++){

                lista.add(new DialogMenuItem(("Procedimiento: 0"+i),R.drawable.ic_process));

            }

            final NormalListDialog dialog = new NormalListDialog(context, lista);
            dialog.title("Procedimientos")//
                    .titleTextSize_SP(18)//
                    .titleTextColor(Color.WHITE)
                    .titleBgColor(Color.BLACK)//
                    .itemPressColor(Color.parseColor("#85D3EF"))//
                    .itemTextColor(Color.parseColor("#303030"))//
                    .itemTextSize(14)//

                    .cornerRadius(0)//
                    .widthScale(0.8f)//

                    .show();

            dialog.setOnOperItemClickL(new OnOperItemClickL() {
                @Override
                public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                   //T.showShort(context, mMenuItems.get(position).mOperName);
                    dialog.dismiss();
                }
            });
        }
    }

}
